<?php

class Pembayaran_rawat_inapController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model=new TindakanRawatInap('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatInap']))
			$model->attributes=$_GET['TindakanRawatInap'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionDetail()
	{
		$id = Yii::app()->request->getQuery('id');

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('detail',array(
			'model'=>$this->loadModel($id),
			'model_jalan'=>$this->loadModelJalan($id),
			'model_darurat'=>$this->loadModelDarurat($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_detail_jalan'=>$this->loadModelDetailJalan($id),
			'model_detail_darurat'=>$this->loadModelDetailDarurat($id),
			'model_pembayaran'=>$this->loadModelPembayaran($id),
			'nama_dokter' => $nama_dokter,
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
		));
	}

	public function actionCetak()
	{
		$id = Yii::app()->request->getQuery('id');

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('cetak',array(
			'model'=>$this->loadModel($id),
			'model_jalan'=>$this->loadModelJalan($id),
			'model_darurat'=>$this->loadModelDarurat($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_detail_jalan'=>$this->loadModelDetailJalan($id),
			'model_detail_darurat'=>$this->loadModelDetailDarurat($id),
			'model_pembayaran'=>$this->loadModelPembayaran($id),
			'nama_dokter' => $nama_dokter,
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
		));
	}

	public function actionCreate()
	{
		$id = Yii::app()->request->getQuery('id');

		$model=new PembayaranRawatInap;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PembayaranRawatInap']))
		{
			$model->attributes=$_POST['PembayaranRawatInap'];
			if($model->save())
			{
				?>
				<script type="text/javascript">
               window.parent.location.reload();
				</script>
				<?php
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'id'=>$id
		));
	}

	public function actionDelete()
	{
		$id = Yii::app()->request->getQuery('id');
		$this->loadModelDetailPembayaran($id)->delete();
		?>
		<script type="text/javascript">
		window.history.back()
		</script>
		<?php
	}

	public function loadModel($id)
	{
		$model=TindakanRawatInap::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelJalan($id)
	{
		$model=TindakanRawatJalan::model()->findAll(array("condition"=>"id_sumber = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDarurat($id)
	{
		$model=TindakanRawatDarurat::model()->findAll(array("condition"=>"id_sumber = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetail($id)
	{
		$model=PemakaianAlatRawatInap::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_inap = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailTindakan($id)
	{
		$model=ManajamenTindakanRawatInap::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_inap = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailJalan($id)
	{
		$model=PemakaianAlatRawatJalan::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_jalan = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailDarurat($id)
	{
		$model=PemakaianAlatRawatDarurat::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_darurat = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelPembayaran($id)
	{
		$model=PembayaranRawatInap::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_inap = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailPembayaran($id)
	{
		$model=PembayaranRawatInap::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogPenyakit($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
		$model=LogPenyakitRawatInap::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokter($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
		$model=LogDokterRawatInap::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokterJalan($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_jalan = '".$id."'";
		$model=LogDokterRawatJalan::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokterDarurat($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogDokterRawatDarurat::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}