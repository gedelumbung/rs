<?php

class Tindakan_rawat_daruratController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete', 'pindah', 'cetak'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$penyakit = $this->loadModelLogPenyakit($id);
		$nama_penyakit = "";
		foreach($penyakit as $data)
		{
			$nama_penyakit .= '<p>'.$data->Penyakit->penyakit.'</p>';
		}

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'penyakit'=>$nama_penyakit,
			'nama_dokter'=>$nama_dokter,
		));
	}

	public function actionCetak($id)
	{
		$penyakit = $this->loadModelLogPenyakit($id);
		$nama_penyakit = "";
		foreach($penyakit as $data)
		{
			$nama_penyakit .= '<p>'.$data->Penyakit->penyakit.'</p>';
		}

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('cetak',array(
			'model'=>$this->loadModel($id),
			'penyakit'=>$nama_penyakit,
			'nama_dokter'=>$nama_dokter,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TindakanRawatDarurat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TindakanRawatDarurat']))
		{
			$model->attributes=$_POST['TindakanRawatDarurat'];
			$model->id_user = Yii::app()->user->id;
			$model->sumber = 'main';
			$model->id_sumber = 0;

			if($model->save())
			{
				if($_POST['TindakanRawatDarurat']['id_penyakit']!=="")
				{
					$id_penyakit = "";
					$count_penyakit = count($_POST['TindakanRawatDarurat']['id_penyakit']);
					for($i=0;$i<$count_penyakit;$i++)
					{
						$model_log_penyakit=new LogPenyakitRawatDarurat();
						$model_log_penyakit->id_tindakan_rawat_darurat = $model->id_tr_tindakan_rawat_darurat;
						$model_log_penyakit->id_penyakit = $_POST['TindakanRawatDarurat']['id_penyakit'][$i];
						$model_log_penyakit->save();
					}
				}

				if($_POST['TindakanRawatDarurat']['id_dokter']!=="")
				{
					$id_dokter = "";
					$count_dokter = count($_POST['TindakanRawatDarurat']['id_dokter']);
					for($i=0;$i<$count_dokter;$i++)
					{
						$model_log_dokter=new LogDokterRawatDarurat();
						$model_log_dokter->id_tindakan_rawat_darurat = $model->id_tr_tindakan_rawat_darurat;
						$model_log_dokter->id_dokter = $_POST['TindakanRawatDarurat']['id_dokter'][$i];
						$model_log_dokter->save();
					}
				}

				$this->redirect(array('view','id'=>$model->id_tr_tindakan_rawat_darurat));
			}
		}

		$ex_dokter = explode(",", $model->id_dokter);
		$model->id_dokter = $ex_dokter;

		$ex_penyakit = explode(",", $model->id_penyakit);
		$model->id_penyakit = $ex_penyakit;

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TindakanRawatDarurat']))
		{
			$model->attributes=$_POST['TindakanRawatDarurat'];

			if($model->save())
			{
				
				if($_POST['TindakanRawatDarurat']['id_penyakit']!=="")
				{
					Yii::app()->db->createCommand("delete from tbl_log_penyakit_rawat_darurat where id_tindakan_rawat_darurat='".$id."'")->execute();
					$id_penyakit = "";
					$count_penyakit = count($_POST['TindakanRawatDarurat']['id_penyakit']);
					for($i=0;$i<$count_penyakit;$i++)
					{
						$model_log_penyakit=new LogPenyakitRawatDarurat();
						$model_log_penyakit->id_tindakan_rawat_darurat = $model->id_tr_tindakan_rawat_darurat;
						$model_log_penyakit->id_penyakit = $_POST['TindakanRawatDarurat']['id_penyakit'][$i];
						$model_log_penyakit->save();
					}
				}

				if($_POST['TindakanRawatDarurat']['id_dokter']!=="")
				{
					Yii::app()->db->createCommand("delete from tbl_log_dokter_rawat_darurat where id_tindakan_rawat_darurat='".$id."'")->execute();
					$id_dokter = "";
					$count_dokter = count($_POST['TindakanRawatDarurat']['id_dokter']);
					for($i=0;$i<$count_dokter;$i++)
					{
						$model_log_dokter=new LogDokterRawatDarurat();
						$model_log_dokter->id_tindakan_rawat_darurat = $model->id_tr_tindakan_rawat_darurat;
						$model_log_dokter->id_dokter = $_POST['TindakanRawatDarurat']['id_dokter'][$i];
						$model_log_dokter->save();
					}
				}
				$this->redirect(array('view','id'=>$model->id_tr_tindakan_rawat_darurat));

			}
		}

		$dokter = $this->loadModelLogDokter($id);
		$id_dokter_param = array();
		foreach($dokter as $data)
		{
			array_push($id_dokter_param, $data->id_dokter);
		}

		$model->id_dokter = $id_dokter_param;

		$penyakit = $this->loadModelLogPenyakit($id);
		$id_penyakit_param = array();
		foreach($penyakit as $data)
		{
			array_push($id_penyakit_param, $data->id_penyakit);
		}

		$model->id_penyakit = $id_penyakit_param;

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionPindah($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TindakanRawatDarurat']))
		{
			if($_POST['TindakanRawatDarurat']['pindah']=="inap")
			{
				$model_inap=new TindakanRawatInap;
				$model_inap->attributes=$_POST['TindakanRawatDarurat'];
				$model_inap->id_user = Yii::app()->user->id;
				$model_inap->sumber = 'darurat';
				$model_inap->id_sumber = $id;

				if($model_inap->save())
				{
					if($_POST['TindakanRawatDarurat']['id_penyakit']!=="")
					{
						$id_penyakit = "";
						$count_penyakit = count($_POST['TindakanRawatDarurat']['id_penyakit']);
						for($i=0;$i<$count_penyakit;$i++)
						{
							$model_log_penyakit=new LogPenyakitRawatInap();
							$model_log_penyakit->id_tindakan_rawat_inap = $model_inap->id_tr_tindakan_rawat_inap;
							$model_log_penyakit->id_penyakit = $_POST['TindakanRawatDarurat']['id_penyakit'][$i];
							$model_log_penyakit->save();
						}
					}

					if($_POST['TindakanRawatDarurat']['id_dokter']!=="")
					{
						$id_dokter = "";
						$count_dokter = count($_POST['TindakanRawatDarurat']['id_dokter']);
						for($i=0;$i<$count_dokter;$i++)
						{
							$model_log_dokter=new LogDokterRawatInap();
							$model_log_dokter->id_tindakan_rawat_inap = $model_inap->id_tr_tindakan_rawat_inap;
							$model_log_dokter->id_dokter = $_POST['TindakanRawatDarurat']['id_dokter'][$i];
							$model_log_dokter->save();
						}
					}
				}
				$this->redirect(array("tindakan_rawat_inap/".$model_inap->id_tr_tindakan_rawat_inap." "));
			}
			else
			{
				$model_jalan=new TindakanRawatJalan;
				$model_jalan->attributes=$_POST['TindakanRawatDarurat'];
				$model_jalan->id_user = Yii::app()->user->id;
				$model_jalan->sumber = 'jalan';
				$model_jalan->id_sumber = $id;

				if($model_jalan->save())
				{
					if($_POST['TindakanRawatDarurat']['id_penyakit']!=="")
					{
						$id_penyakit = "";
						$count_penyakit = count($_POST['TindakanRawatDarurat']['id_penyakit']);
						for($i=0;$i<$count_penyakit;$i++)
						{
							$model_log_penyakit=new LogPenyakitRawatJalan();
							$model_log_penyakit->id_tindakan_rawat_jalan = $model_jalan->id_tr_tindakan_rawat_jalan;
							$model_log_penyakit->id_penyakit = $_POST['TindakanRawatDarurat']['id_penyakit'][$i];
							$model_log_penyakit->save();
						}
					}

					if($_POST['TindakanRawatDarurat']['id_dokter']!=="")
					{
						$id_dokter = "";
						$count_dokter = count($_POST['TindakanRawatDarurat']['id_dokter']);
						for($i=0;$i<$count_dokter;$i++)
						{
							$model_log_dokter=new LogDokterRawatJalan();
							$model_log_dokter->id_tindakan_rawat_jalan = $model_jalan->id_tr_tindakan_rawat_jalan;
							$model_log_dokter->id_dokter = $_POST['TindakanRawatDarurat']['id_dokter'][$i];
							$model_log_dokter->save();
						}
					}

				}
				$this->redirect(array("tindakan_rawat_jalan/".$model_jalan->id_tr_tindakan_rawat_jalan." "));
			}
		}

		$dokter = $this->loadModelLogDokter($id);
		$id_dokter_param = array();
		foreach($dokter as $data)
		{
			array_push($id_dokter_param, $data->id_dokter);
		}

		$model->id_dokter = $id_dokter_param;

		$penyakit = $this->loadModelLogPenyakit($id);
		$id_penyakit_param = array();
		foreach($penyakit as $data)
		{
			array_push($id_penyakit_param, $data->id_penyakit);
		}

		$model->id_penyakit = $id_penyakit_param;

		$this->render('pindah',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new TindakanRawatDarurat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatDarurat']))
			$model->attributes=$_GET['TindakanRawatDarurat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TindakanRawatDarurat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatDarurat']))
			$model->attributes=$_GET['TindakanRawatDarurat'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TindakanRawatDarurat the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TindakanRawatDarurat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadDokter($id)
	{
		$model=Dokter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadPenyakit($id)
	{
		$model=Penyakit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogPenyakit($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogPenyakitRawatDarurat::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokter($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogDokterRawatDarurat::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TindakanRawatDarurat $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tindakan-rawat-darurat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
