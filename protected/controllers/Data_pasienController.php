<?php

class Data_pasienController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest || Yii::app()->user->status!="administrasi" && Yii::app()->user->status!="apotik" && Yii::app()->user->status!="ugd" && Yii::app()->user->status!="registrasi") 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','cetak','kartu','kartu_pasien'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionCetak($id)
	{
		$this->render('excel',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionKartu($id)
	{
		$this->render('kartu',array(
			'model'=>$this->loadModel($id),
		));
	}
	public function actionKartu_Pasien($id)
	{
		$this->render('kartu_pasien',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pasien;
		$model->no_rm = $this->generateAutoNumber();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pasien']))
		{
			$model->attributes=$_POST['Pasien'];
			$model->tgl_daftar=date('Y-m-d');
			if($model->save())
				$this->redirect(array('cetak','id'=>$model->id_pasien));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pasien']))
		{
			$model->attributes=$_POST['Pasien'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_pasien));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pasien']))
			$model->attributes=$_GET['Pasien'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pasien('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pasien']))
			$model->attributes=$_GET['Pasien'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pasien the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pasien::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pasien $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pasien-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function generateAutoNumber()
	{
		$max = Yii::app()->db->createCommand("SELECT MAX( SUBSTRING( no_rm, 3, 8 ) ) saved_date, MAX(no_rm) max_increment FROM `tbl_data_pasien`")->queryRow();

		if(date('Ymd') === $max['saved_date'])
		{
			$code = substr($max['max_increment'], 0, 2);

			$number = substr($max['max_increment'], 10, 4);

			$new_number = str_repeat("0", 4 - strlen($number+1)).($number+1);

			return $code.date('Ymd').$new_number;
		}
		return 'RM'.date('Ymd').'0001';
	}
}
