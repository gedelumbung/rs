<?php

class Ranking_penyakit_rawat_jalanController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model=Yii::app()->db->createCommand("SELECT c.penyakit, count(c.penyakit) as total FROM `tbl_log_penyakit_rawat_jalan` a left join tbl_tr_tindakan_rawat_jalan b on a.id_tindakan_rawat_jalan=b.id_tr_tindakan_rawat_jalan left join tbl_penyakit c on a.id_penyakit=c.id_penyakit group by a.id_penyakit order by total DESC")->queryAll();

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionExcel()
	{
		$tgl = $_POST['tahun'];
		if(!empty($_POST['bulan']))
		{
			$tgl = $_POST['tahun'].'-'.$_POST['bulan'];
		}


		$model=Yii::app()->db->createCommand("SELECT c.penyakit, count(c.penyakit) as total FROM `tbl_log_penyakit_rawat_jalan` a left join tbl_tr_tindakan_rawat_jalan b on a.id_tindakan_rawat_jalan=b.id_tr_tindakan_rawat_jalan left join tbl_penyakit c on a.id_penyakit=c.id_penyakit where b.tgl_masuk like '%".$tgl."%' group by a.id_penyakit order by total DESC")->queryAll();

		$this->renderPartial('excel',array(
			'model'=>$model,
		));
	}

}