<?php

class Penyakit_pasien_rawat_jalanController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model=new TindakanRawatJalan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatJalan']))
			$model->attributes=$_GET['TindakanRawatJalan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionExcel()
	{
		$model=new TindakanRawatJalan('search_report_tahun');
		$model->tgl_masuk = $_POST['tahun'];
		if(isset($_POST['bulan']))
		{
			$model=new TindakanRawatJalan('search_report_bulan');
			$model->tgl_masuk = $_POST['tahun'].'-'.$_POST['bulan'];
		}

		$this->render('excel',array(
			'model'=>$model,
		));
	}

}