<?php

class Pembayaran_rawat_daruratController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model=new TindakanRawatDarurat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatDarurat']))
			$model->attributes=$_GET['TindakanRawatDarurat'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionDetail()
	{
		$id = Yii::app()->request->getQuery('id');

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('detail',array(
			'model'=>$this->loadModel($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_pembayaran'=>$this->loadModelPembayaran($id),
			'nama_dokter' => $nama_dokter,
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
		));
	}

	public function actionCetak()
	{
		$id = Yii::app()->request->getQuery('id');

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('cetak',array(
			'model'=>$this->loadModel($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_pembayaran'=>$this->loadModelPembayaran($id),
			'nama_dokter' => $nama_dokter,
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
		));
	}

	public function actionCreate()
	{
		$id = Yii::app()->request->getQuery('id');

		$model=new PembayaranRawatDarurat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PembayaranRawatDarurat']))
		{
			$model->attributes=$_POST['PembayaranRawatDarurat'];
			if($model->save())
			{
				?>
				<script type="text/javascript">
               window.parent.location.reload();
				</script>
				<?php
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'id'=>$id
		));
	}

	public function actionDelete()
	{
		$id = Yii::app()->request->getQuery('id');
		$this->loadModelDetailPembayaran($id)->delete();
		?>
		<script type="text/javascript">
		window.history.back()
		</script>
		<?php
	}

	public function loadModel($id)
	{
		$model=TindakanRawatDarurat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetail($id)
	{
		$model=PemakaianAlatRawatDarurat::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_darurat = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailTindakan($id)
	{
		$model=ManajamenTindakanRawatDarurat::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_darurat = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelPembayaran($id)
	{
		$model=PembayaranRawatDarurat::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_darurat = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailPembayaran($id)
	{
		$model=PembayaranRawatDarurat::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogPenyakit($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogPenyakitRawatDarurat::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokter($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogDokterRawatDarurat::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}