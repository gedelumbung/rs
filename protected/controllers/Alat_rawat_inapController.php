<?php

class Alat_rawat_inapController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PemakaianAlatRawatInap;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PemakaianAlatRawatInap']))
		{
			$model->attributes=$_POST['PemakaianAlatRawatInap'];
			if($model->save())
			{

				$updateObat=Yii::app()->db->createCommand("update tbl_obat set pemakaian=pemakaian+".$_POST['PemakaianAlatRawatInap']['jumlah_obat'].", stok_awal = stok_awal-".$_POST['PemakaianAlatRawatInap']['jumlah_obat']." where id_obat='".$_POST['PemakaianAlatRawatInap']['id_obat']."'")->execute();

				$this->redirect(array('view','id'=>$model->id_pemakaian_alat_rawat_inap));

			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$pemakaian_sebelumnya = $model->jumlah_obat;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PemakaianAlatRawatInap']))
		{
			$model->attributes=$_POST['PemakaianAlatRawatInap'];
			if($model->save())
			{
				$updateObat=Yii::app()->db->createCommand("update tbl_obat set pemakaian=pemakaian-".$pemakaian_sebelumnya."+".$_POST['PemakaianAlatRawatInap']['jumlah_obat'].", stok_awal = stok_awal+".$pemakaian_sebelumnya."-".$_POST['PemakaianAlatRawatInap']['jumlah_obat']." where id_obat='".$_POST['PemakaianAlatRawatInap']['id_obat']."'")->execute();
				
				$this->redirect(array('view','id'=>$model->id_pemakaian_alat_rawat_inap));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PemakaianAlatRawatInap('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PemakaianAlatRawatInap']))
			$model->attributes=$_GET['PemakaianAlatRawatInap'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PemakaianAlatRawatInap('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PemakaianAlatRawatInap']))
			$model->attributes=$_GET['PemakaianAlatRawatInap'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PemakaianAlatRawatInap the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PemakaianAlatRawatInap::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PemakaianAlatRawatInap $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pemakaian-alat-rawat-inap-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
