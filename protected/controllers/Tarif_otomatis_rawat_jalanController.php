<?php

class Tarif_otomatis_rawat_jalanController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model=new TindakanRawatJalan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TindakanRawatJalan']))
			$model->attributes=$_GET['TindakanRawatJalan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionDetail()
	{
		$id = Yii::app()->request->getQuery('id');
		
		$penyakit = $this->loadModelLogPenyakit($id);
		$nama_penyakit = "";
		foreach($penyakit as $data)
		{
			$nama_penyakit .= '<p>'.$data->Penyakit->penyakit.'</p>';
		}

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
			'nama_dokter' => $nama_dokter,
			'penyakit' => $nama_penyakit,
		));
	}

	public function actionCetak()
	{
		$id = Yii::app()->request->getQuery('id');
		
		$penyakit = $this->loadModelLogPenyakit($id);
		$nama_penyakit = "";
		foreach($penyakit as $data)
		{
			$nama_penyakit .= '<p>'.$data->Penyakit->penyakit.'</p>';
		}

		$dokter = $this->loadModelLogDokter($id);
		$nama_dokter = "";
		foreach($dokter as $data)
		{
			$nama_dokter .= '<p>'.$data->Dokter->nama_dokter.'</p>';
		}

		$this->render('cetak',array(
			'model'=>$this->loadModel($id),
			'model_detail'=>$this->loadModelDetail($id),
			'model_detail_tindakan'=>$this->loadModelDetailTindakan($id),
			'nama_dokter' => $nama_dokter,
			'penyakit' => $nama_penyakit,
		));
	}

	public function loadModel($id)
	{
		$model=TindakanRawatJalan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetail($id)
	{
		$model=PemakaianAlatRawatJalan::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_jalan = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelDetailTindakan($id)
	{
		$model=ManajamenTindakanRawatJalan::model()->findAll(array("condition"=>"id_tr_tindakan_rawat_jalan = '".$id."' "));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadDokter($id)
	{
		$model=Dokter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogPenyakit($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_jalan = '".$id."'";
		$model=LogPenyakitRawatJalan::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelLogDokter($id)
	{
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_jalan = '".$id."'";
		$model=LogDokterRawatJalan::model()->findAll($criteria);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function loadPenyakit($id)
	{
		$model=Penyakit::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
}