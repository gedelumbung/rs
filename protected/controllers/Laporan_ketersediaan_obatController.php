<?php

class Laporan_ketersediaan_obatController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model = Yii::app()->db->createCommand("SELECT nama_obat, kemasan, harga, stok_awal, penerimaan, ((select sum(jumlah_obat) as jum_a from tbl_pemakaian_alat_rawat_darurat where id_obat=z.id_obat)+(select sum(jumlah_obat) as jum_b from tbl_pemakaian_alat_rawat_inap where id_obat=z.id_obat)+(select sum(jumlah_obat) as jum_c from tbl_pemakaian_alat_rawat_jalan where id_obat=z.id_obat)) as pemakaian_habis, (penerimaan+stok_awal) as stok_akhir, ((penerimaan+stok_awal-pemakaian)*harga) as jumlah, keterangan, exp_date from tbl_obat z")->queryAll();
		$this->render('index', array(
			'model' => $model
			));
	}


	public function actionExcel()
	{
		$tgl = $_POST['tahun'];
		if(!empty($_POST['bulan']))
		{
			$tgl = $_POST['tahun'].'-'.$_POST['bulan'];
		}

		$model = Yii::app()->db->createCommand("SELECT nama_obat, kemasan, harga, stok_awal, penerimaan, ((select sum(jumlah_obat) as jum_a from tbl_pemakaian_alat_rawat_darurat where tanggal like '%".$tgl."%' and id_obat=z.id_obat)+(select sum(jumlah_obat) as jum_b from tbl_pemakaian_alat_rawat_inap where tanggal like '%".$tgl."%' and id_obat=z.id_obat)+(select sum(jumlah_obat) as jum_c from tbl_pemakaian_alat_rawat_jalan where tanggal like '%".$tgl."%' and id_obat=z.id_obat)) as pemakaian_habis, (penerimaan+stok_awal) as stok_akhir, ((penerimaan+stok_awal-pemakaian)*harga) as jumlah, keterangan, exp_date from tbl_obat z")->queryAll();
		$this->renderPartial('excel', array(
			'model' => $model
			));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}