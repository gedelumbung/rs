<?php

class Register_rawat_inapController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex()
	{
		$model=new PemakaianAlatRawatInap('search_report');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PemakaianAlatRawatInap']))
			$model->attributes=$_GET['PemakaianAlatRawatInap'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionExcel()
	{
		$model=new PemakaianAlatRawatInap('search_report_tahun');
		$model->tanggal = $_POST['tahun'];
		if(isset($_POST['bulan']))
		{
			$model=new PemakaianAlatRawatInap('search_report_bulan');
			$model->tanggal = $_POST['tahun'].'-'.$_POST['bulan'];
		}

		$this->render('excel',array(
			'model'=>$model,
		));
	}
}