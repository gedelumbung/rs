<?php

class Laporan_pemakaian_obat_rawat_jalanController extends Controller
{
	public function init()
	{
		if (Yii::app()->user->isGuest) 
		{
			$this->redirect(array("site/index"));
		}
	}
	
	public $layout='main_dashboard';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function actionIndex()
	{
		$model=new PemakaianAlatRawatJalan('search_report');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PemakaianAlatRawatJalan']))
			$model->attributes=$_GET['PemakaianAlatRawatJalan'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionExcel()
	{
		$model=new PemakaianAlatRawatJalan('search_report_tahun');
		$model->tanggal = $_POST['tahun'];
		if(isset($_POST['bulan']))
		{
			$model=new PemakaianAlatRawatJalan('search_report_bulan');
			$model->tanggal = $_POST['tahun'].'-'.$_POST['bulan'];
		}

		$this->render('excel',array(
			'model'=>$model,
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}