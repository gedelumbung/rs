<?php

/**
 * This is the model class for table "tbl_tindakan_rawat_jalan".
 *
 * The followings are the available columns in table 'tbl_tindakan_rawat_jalan':
 * @property integer $id_tindakan_rawat_jalan
 * @property integer $id_tr_tindakan_rawat_jalan
 * @property string $tanggal
 */
class ManajamenTindakanRawatJalan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tindakan_rawat_jalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tr_tindakan_rawat_jalan, tanggal, id_tindakan', 'required'),
			array('id_tr_tindakan_rawat_jalan', 'numerical', 'integerOnly'=>true),
			array('tanggal', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan_rawat_jalan, id_tr_tindakan_rawat_jalan, tanggal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'TindakanRawatJalan'=>array(self::BELONGS_TO,'TindakanRawatJalan','id_tr_tindakan_rawat_jalan'),
			'Tindakan'=>array(self::BELONGS_TO,'Tindakan','id_tindakan'),
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan_rawat_jalan' => 'Id Tindakan Rawat Jalan',
			'id_tr_tindakan_rawat_jalan' => 'Tindakan Rawat Jalan',
			'id_tindakan' => 'Tindakan',
			'tanggal' => 'Tanggal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tindakan_rawat_jalan',$this->id_tindakan_rawat_jalan);
		$criteria->compare('id_tr_tindakan_rawat_jalan',$this->id_tr_tindakan_rawat_jalan);
		$criteria->compare('tanggal',$this->tanggal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManajamenTindakanRawatJalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
