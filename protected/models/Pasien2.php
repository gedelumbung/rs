<?php

/**
 * This is the model class for table "tbl_pasien".
 *
 * The followings are the available columns in table 'tbl_pasien':
 * @property integer $id_pasien
 * @property string $nama
 * @property string $nama_alias
 * @property string $jk
 * @property string $tgl_lahir
 * @property string $nik_pasien
 * @property string $alamat
 * @property string $telpon_home
 * @property string $telpon_mobile
 * @property string $pekerjaan
 * @property string $status
 * @property string $program_jaminan
 * @property string $no_sep_no_sktm
 * @property string $no_kartu
 * @property string $asal_rujukan
 */
class Pasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, nama_alias, jk, tgl_lahir, nik_pasien, alamat, telpon_home, telpon_mobile, pekerjaan, status, program_jaminan, no_sep_no_sktm, no_kartu, asal_rujukan', 'required'),
			array('nama, nama_alias', 'length', 'max'=>150),
			array('jk', 'length', 'max'=>15),
			array('tgl_lahir, status, program_jaminan', 'length', 'max'=>50),
			array('nik_pasien, telpon_home, telpon_mobile, no_sep_no_sktm, no_kartu', 'length', 'max'=>75),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pasien, nama, nama_alias, jk, tgl_lahir, nik_pasien, alamat, telpon_home, telpon_mobile, pekerjaan, status, program_jaminan, no_sep_no_sktm, no_kartu, asal_rujukan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pasien' => 'Id Pasien',
			'nama' => 'Nama',
			'nama_alias' => 'Nama Alias',
			'jk' => 'Jk',
			'tgl_lahir' => 'Tgl Lahir',
			'nik_pasien' => 'Nik Pasien',
			'alamat' => 'Alamat',
			'telpon_home' => 'Telpon Home',
			'telpon_mobile' => 'Telpon Mobile',
			'pekerjaan' => 'Pekerjaan',
			'status' => 'Status',
			'program_jaminan' => 'Program Jaminan',
			'no_sep_no_sktm' => 'No Sep No Sktm',
			'no_kartu' => 'No Kartu',
			'asal_rujukan' => 'Asal Rujukan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pasien',$this->id_pasien);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nama_alias',$this->nama_alias,true);
		$criteria->compare('jk',$this->jk,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('nik_pasien',$this->nik_pasien,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telpon_home',$this->telpon_home,true);
		$criteria->compare('telpon_mobile',$this->telpon_mobile,true);
		$criteria->compare('pekerjaan',$this->pekerjaan,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('program_jaminan',$this->program_jaminan,true);
		$criteria->compare('no_sep_no_sktm',$this->no_sep_no_sktm,true);
		$criteria->compare('no_kartu',$this->no_kartu,true);
		$criteria->compare('asal_rujukan',$this->asal_rujukan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getConcatened()
	{
	   return $this->id_pasien.'. '.$this->nama;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
