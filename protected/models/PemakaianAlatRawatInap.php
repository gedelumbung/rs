<?php

/**
 * This is the model class for table "tbl_pemakaian_alat_rawat_inap".
 *
 * The followings are the available columns in table 'tbl_pemakaian_alat_rawat_inap':
 * @property integer $id_pemakaian_alat_rawat_inap
 * @property integer $id_tr_tindakan_rawat_inap
 * @property integer $id_obat
 * @property integer $jumlah_obat
 * @property string $tanggal
 * @property string $keterangan
 */
class PemakaianAlatRawatInap extends CActiveRecord
{
  public $id_tipe_registrasi;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_pemakaian_alat_rawat_inap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tr_tindakan_rawat_inap, id_obat, jumlah_obat, tanggal, keterangan', 'required'),
			array('id_tr_tindakan_rawat_inap, id_obat, jumlah_obat', 'numerical', 'integerOnly'=>true),
			array('tanggal', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pemakaian_alat_rawat_inap, id_tipe_registrasi, id_tr_tindakan_rawat_inap, id_obat, jumlah_obat, tanggal, keterangan', 'safe', 'on'=>'search'),
			array('id_pemakaian_alat_rawat_inap, id_tipe_registrasi, id_tr_tindakan_rawat_inap, id_obat, jumlah_obat, tanggal, keterangan', 'safe', 'on'=>'search_report'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'TindakanRawatInap'=>array(self::BELONGS_TO,'TindakanRawatInap','id_tr_tindakan_rawat_inap'),
			'Obat'=>array(self::BELONGS_TO,'Obat','id_obat')
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pemakaian_alat_rawat_inap' => 'Id',
			'id_tr_tindakan_rawat_inap' => 'Nama Pasien',
			'id_obat' => 'Obat',
			'jumlah_obat' => 'Jumlah Obat',
			'tanggal' => 'Tanggal',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pemakaian_alat_rawat_inap',$this->id_pemakaian_alat_rawat_inap);
		$criteria->compare('id_tr_tindakan_rawat_inap',$this->id_tr_tindakan_rawat_inap);
		$criteria->compare('id_obat',$this->id_obat);
		$criteria->compare('jumlah_obat',$this->jumlah_obat);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_report()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'TindakanRawatInap' );
		$criteria->compare( 'TindakanRawatInap.id_tipe_registrasi', $this->id_tipe_registrasi, true );

		$criteria->compare('id_pemakaian_alat_rawat_inap',$this->id_pemakaian_alat_rawat_inap);
		$criteria->compare('id_tr_tindakan_rawat_inap',$this->id_tr_tindakan_rawat_inap);
		$criteria->compare('id_obat',$this->id_obat);
		$criteria->compare('jumlah_obat',$this->jumlah_obat);
		$criteria->compare('tanggal',substr($this->tanggal,0,7),true);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_report_bulan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'TindakanRawatInap' );
		$criteria->compare('tanggal',$this->tanggal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_report_tahun()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'TindakanRawatInap' );
		$criteria->compare('tanggal',$this->tanggal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PemakaianAlatRawatInap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
