<?php

/**
 * This is the model class for table "tbl_tr_tindakan_rawat_jalan".
 *
 * The followings are the available columns in table 'tbl_tr_tindakan_rawat_jalan':
 * @property integer $id_tr_tindakan_rawat_jalan
 * @property string $tipe_registrasi
 * @property integer $id_pasien
 * @property string $tgl_masuk
 * @property string $jam_masuk
 * @property string $keterangan_tindakan
 * @property integer $id_dokter
 * @property string $diagnosa_primer
 * @property integer $id_obat
 * @property integer $jumlah_obat
 * @property integer $id_info_kamar
 */
class TindakanRawatJalan extends CActiveRecord
{
  public $nama_pasien;
  public $pindah;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tr_tindakan_rawat_jalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tipe_registrasi, id_pasien, tgl_masuk, jam_masuk, diagnosa_primer, diagnosa_sekunder', 'required'),
			array('id_tipe_registrasi, tgl_masuk, jam_masuk', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tr_tindakan_rawat_jalan, status_pasien, id_tipe_registrasi, nama_pasien, tgl_masuk, jam_masuk, keterangan_tindakan, id_dokter, diagnosa_primer, id_info_kamar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'Penyakit'=>array(self::BELONGS_TO,'Penyakit','id_penyakit'),
			'TipeRegistrasi'=>array(self::BELONGS_TO,'TipeRegistrasi','id_tipe_registrasi'),
			'Pasien'=>array(self::BELONGS_TO,'Pasien','id_pasien'),
			'Dokter'=>array(self::BELONGS_TO,'Dokter','id_dokter'),
			'User'=>array(self::BELONGS_TO,'UserCmsModel','id_user'),
			'Kamar'=>array(self::BELONGS_TO,'Kamar','id_info_kamar')
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tr_tindakan_rawat_jalan' => 'Id Rekam Medik Rawat Jalan',
			'id_tipe_registrasi' => 'Tipe Registrasi',
			'id_pasien' => 'No Rm',
			'tgl_masuk' => 'Tanggal Masuk',
			'jam_masuk' => 'Jam Masuk',
			'tgl_keluar' => 'Tanggal Keluar',
			'id_dokter' => 'Dokter',
			'diagnosa_primer' => 'Diagnosa Primer',
			'diagnosa_sekunder' => 'Diagnosa Sekunder',
			'id_info_kamar' => 'Info Kamar',
			'nama_pasien' => 'Info Kamar',
			'id_penyakit' => 'Penyakit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'Pasien' );
		$criteria->compare( 'Pasien.nama', $this->nama_pasien, true );

		$criteria->compare('id_tr_tindakan_rawat_jalan',$this->id_tr_tindakan_rawat_jalan);
		$criteria->compare('id_tipe_registrasi',$this->id_tipe_registrasi,true);
		$criteria->compare('id_pasien',$this->id_pasien);
		$criteria->compare('tgl_masuk',$this->tgl_masuk,true);
		$criteria->compare('jam_masuk',$this->jam_masuk,true);
		$criteria->compare('keterangan_tindakan',$this->keterangan_tindakan,true);
		$criteria->compare('id_dokter',$this->id_dokter);
		$criteria->compare('diagnosa_primer',$this->diagnosa_primer,true);
		$criteria->compare('id_info_kamar',$this->id_info_kamar);
		$criteria->compare('status_pasien',$this->status_pasien, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_report_bulan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'Pasien' );
		$criteria->compare( 'Pasien.nama', $this->nama_pasien, true );
		
		$criteria->compare('tgl_masuk',$this->tgl_masuk,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_report_tahun()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array( 'Pasien' );
		$criteria->compare( 'Pasien.nama', $this->nama_pasien, true );
		
		$criteria->compare('tgl_masuk',$this->tgl_masuk,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_pembayaran()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition = "sumber = 'main'";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getConcatened()
	{
	   return $this->Pasien->nama.' - '.$this->tgl_masuk;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TindakanRawatJalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
