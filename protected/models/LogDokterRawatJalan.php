<?php

/**
 * This is the model class for table "tbl_log_dokter_rawat_jalan".
 *
 * The followings are the available columns in table 'tbl_log_dokter_rawat_jalan':
 * @property integer $id_log_dokter_rawat_jalan
 * @property integer $id_tindakan_rawat_jalan
 * @property integer $id_dokter
 */
class LogDokterRawatJalan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_log_dokter_rawat_jalan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tindakan_rawat_jalan, id_dokter', 'required'),
			array('id_tindakan_rawat_jalan, id_dokter', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_log_dokter_rawat_jalan, id_tindakan_rawat_jalan, id_dokter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'Dokter'=>array(self::BELONGS_TO,'Dokter','id_dokter'),
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_log_dokter_rawat_jalan' => 'Id Log Dokter Rawat Jalan',
			'id_tindakan_rawat_jalan' => 'Id Tindakan Rawat Jalan',
			'id_dokter' => 'Id Dokter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_log_dokter_rawat_jalan',$this->id_log_dokter_rawat_jalan);
		$criteria->compare('id_tindakan_rawat_jalan',$this->id_tindakan_rawat_jalan);
		$criteria->compare('id_dokter',$this->id_dokter);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogDokterRawatJalan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
