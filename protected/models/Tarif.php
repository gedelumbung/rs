<?php

/**
 * This is the model class for table "tbl_info_tarif".
 *
 * The followings are the available columns in table 'tbl_info_tarif':
 * @property integer $id_info_tarif
 * @property string $nama_tarif
 * @property integer $tarif
 */
class Tarif extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_info_tarif';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_tarif, tarif', 'required'),
			array('tarif', 'numerical', 'integerOnly'=>true),
			array('nama_tarif', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_info_tarif, nama_tarif, tarif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_info_tarif' => 'Id Info Tarif',
			'nama_tarif' => 'Nama Tarif',
			'tarif' => 'Tarif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_info_tarif',$this->id_info_tarif);
		$criteria->compare('nama_tarif',$this->nama_tarif,true);
		$criteria->compare('tarif',$this->tarif);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tarif the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
