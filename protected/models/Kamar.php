<?php

/**
 * This is the model class for table "tbl_info_kamar".
 *
 * The followings are the available columns in table 'tbl_info_kamar':
 * @property integer $id_info_kamar
 * @property string $nama_kamar
 * @property string $tipe
 * @property string $harga
 * @property string $keterangan
 */
class Kamar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_info_kamar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kamar, tersedia, tipe, harga, keterangan', 'required'),
			array('nama_kamar', 'length', 'max'=>150),
			array('tipe, harga', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_info_kamar, nama_kamar, tersedia, tipe, harga, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_info_kamar' => 'Id Info Kamar',
			'nama_kamar' => 'Nama Kamar',
			'tipe' => 'Tipe',
			'harga' => 'Harga',
			'keterangan' => 'Keterangan',
			'tersedia' => 'Tersedia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_info_kamar',$this->id_info_kamar);
		$criteria->compare('nama_kamar',$this->nama_kamar,true);
		$criteria->compare('tipe',$this->tipe,true);
		$criteria->compare('harga',$this->harga,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('tersedia',$this->tersedia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kamar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
