<?php

/**
 * This is the model class for table "tbl_log_penyakit_rawat_darurat".
 *
 * The followings are the available columns in table 'tbl_log_penyakit_rawat_darurat':
 * @property integer $id_log_penyakit_rawat_darurat
 * @property integer $id_tindakan_rawat_darurat
 * @property integer $id_penyakit
 */
class LogPenyakitRawatDarurat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_log_penyakit_rawat_darurat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tindakan_rawat_darurat, id_penyakit', 'required'),
			array('id_tindakan_rawat_darurat, id_penyakit', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_log_penyakit_rawat_darurat, id_tindakan_rawat_darurat, id_penyakit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'Penyakit'=>array(self::BELONGS_TO,'Penyakit','id_penyakit'),
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_log_penyakit_rawat_darurat' => 'Id Log Penyakit Rawat Darurat',
			'id_tindakan_rawat_darurat' => 'Id Tindakan Rawat Darurat',
			'id_penyakit' => 'Id Penyakit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_log_penyakit_rawat_darurat',$this->id_log_penyakit_rawat_darurat);
		$criteria->compare('id_tindakan_rawat_darurat',$this->id_tindakan_rawat_darurat);
		$criteria->compare('id_penyakit',$this->id_penyakit);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LogPenyakitRawatDarurat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
