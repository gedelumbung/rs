<?php

/**
 * This is the model class for table "tbl_tindakan_rawat_inap".
 *
 * The followings are the available columns in table 'tbl_tindakan_rawat_inap':
 * @property integer $id_tindakan_rawat_inap
 * @property integer $id_tindakan
 * @property integer $id_tr_tindakan_rawat_inap
 * @property string $tanggal
 */
class ManajamenTindakanRawatInap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tindakan_rawat_inap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tindakan, id_tr_tindakan_rawat_inap, tanggal', 'required'),
			array('id_tindakan, id_tr_tindakan_rawat_inap', 'numerical', 'integerOnly'=>true),
			array('tanggal', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_tindakan_rawat_inap, id_tindakan, id_tr_tindakan_rawat_inap, tanggal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'TindakanRawatInap'=>array(self::BELONGS_TO,'TindakanRawatInap','id_tr_tindakan_rawat_inap'),
			'Tindakan'=>array(self::BELONGS_TO,'Tindakan','id_tindakan'),
			);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_tindakan_rawat_inap' => 'Id Tindakan Rawat Inap',
			'id_tindakan' => 'Tindakan',
			'id_tr_tindakan_rawat_inap' => 'Tindakan Rawat Inap',
			'tanggal' => 'Tanggal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_tindakan_rawat_inap',$this->id_tindakan_rawat_inap);
		$criteria->compare('id_tindakan',$this->id_tindakan);
		$criteria->compare('id_tr_tindakan_rawat_inap',$this->id_tr_tindakan_rawat_inap);
		$criteria->compare('tanggal',$this->tanggal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManajamenTindakanRawatInap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
