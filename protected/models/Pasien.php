<?php

/**
 * This is the model class for table "tbl_data_pasien".
 *
 * The followings are the available columns in table 'tbl_data_pasien':
 * @property integer $id_pasien
 * @property string $nama
 * @property string $nama_keluarga
 * @property string $tempat_lahir
 * @property string $tgl_lahir
 * @property string $jk
 * @property string $alamat
 * @property string $kelurahan
 * @property string $kode_pos
 * @property string $provinsi
 * @property string $telepon_rumah
 * @property string $telepon_hp
 * @property string $nama_perusahaan
 * @property string $alamat_perusahaan
 * @property string $kantor
 * @property string $ext
 * @property string $agama
 * @property string $status
 * @property string $pendidikan
 * @property string $warga_negara
 * @property string $penanggung_jawab
 * @property string $nama_penanggung_jawab
 * @property string $hubungan
 * @property string $alamat_penanggung_jawab
 * @property string $telepon_penanggung_jawab
 * @property string $kartu_identitas_penanggung_jawab
 * @property string $nomor_identitas_penanggung_jawab
 * @property integer $no_ktp
 * @property integer $no_sim
 * @property integer $no_pasport
 */
class Pasien extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_data_pasien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, nama_keluarga, tempat_lahir, tgl_lahir, jk, alamat, kelurahan, kode_pos, provinsi, telepon_rumah, telepon_hp, nama_perusahaan, alamat_perusahaan, kantor, ext, agama, status, pendidikan, warga_negara, penanggung_jawab, nama_penanggung_jawab, hubungan, alamat_penanggung_jawab, telepon_penanggung_jawab, kartu_identitas_penanggung_jawab, nomor_identitas_penanggung_jawab, no_ktp, no_sim, no_pasport, no_rm', 'required'),
			array('nama, nama_keluarga, tempat_lahir, tgl_lahir, kelurahan, provinsi, telepon_rumah, kantor, ext, penanggung_jawab, nama_penanggung_jawab, hubungan', 'length', 'max'=>150),
			array('jk, kode_pos', 'length', 'max'=>20),
			array('telepon_hp, nama_perusahaan, agama, status, telepon_penanggung_jawab, kartu_identitas_penanggung_jawab', 'length', 'max'=>50),
			array('pendidikan, warga_negara, nomor_identitas_penanggung_jawab', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pasien, nama, nama_keluarga, tempat_lahir, tgl_lahir, jk, alamat, kelurahan, kode_pos, provinsi, telepon_rumah, telepon_hp, nama_perusahaan, alamat_perusahaan, kantor, ext, agama, status, pendidikan, warga_negara, penanggung_jawab, nama_penanggung_jawab, hubungan, alamat_penanggung_jawab, telepon_penanggung_jawab, kartu_identitas_penanggung_jawab, nomor_identitas_penanggung_jawab, no_ktp, no_sim, no_pasport', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'no_rm' => 'NO RM',
			'id_pasien' => 'Id Pasien',
			'nama' => 'Nama',
			'nama_keluarga' => 'Nama Keluarga',
			'tempat_lahir' => 'Tempat Lahir',
			'tgl_lahir' => 'Tgl Lahir',
			'jk' => 'Jk',
			'alamat' => 'Alamat',
			'kelurahan' => 'Kelurahan',
			'kode_pos' => 'Kode Pos',
			'provinsi' => 'Provinsi',
			'telepon_rumah' => 'Telepon Rumah',
			'telepon_hp' => 'Telepon Hp',
			'nama_perusahaan' => 'Nama Perusahaan',
			'alamat_perusahaan' => 'Alamat Perusahaan',
			'kantor' => 'Kantor',
			'ext' => 'Ext',
			'agama' => 'Agama',
			'status' => 'Status',
			'pendidikan' => 'Pendidikan',
			'warga_negara' => 'Warga Negara',
			'penanggung_jawab' => 'Penanggung Jawab',
			'nama_penanggung_jawab' => 'Nama Penanggung Jawab',
			'hubungan' => 'Hubungan',
			'alamat_penanggung_jawab' => 'Alamat Penanggung Jawab',
			'telepon_penanggung_jawab' => 'Telepon Penanggung Jawab',
			'kartu_identitas_penanggung_jawab' => 'Kartu Identitas Penanggung Jawab',
			'nomor_identitas_penanggung_jawab' => 'Nomor Identitas Penanggung Jawab',
			'no_ktp' => 'No Ktp',
			'no_sim' => 'No Sim',
			'no_pasport' => 'No Pasport',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pasien',$this->id_pasien);
		$criteria->compare('no_rm',$this->no_rm, true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('nama_keluarga',$this->nama_keluarga,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tgl_lahir',$this->tgl_lahir,true);
		$criteria->compare('jk',$this->jk,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kelurahan',$this->kelurahan,true);
		$criteria->compare('kode_pos',$this->kode_pos,true);
		$criteria->compare('provinsi',$this->provinsi,true);
		$criteria->compare('telepon_rumah',$this->telepon_rumah,true);
		$criteria->compare('telepon_hp',$this->telepon_hp,true);
		$criteria->compare('nama_perusahaan',$this->nama_perusahaan,true);
		$criteria->compare('alamat_perusahaan',$this->alamat_perusahaan,true);
		$criteria->compare('kantor',$this->kantor,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('warga_negara',$this->warga_negara,true);
		$criteria->compare('penanggung_jawab',$this->penanggung_jawab,true);
		$criteria->compare('nama_penanggung_jawab',$this->nama_penanggung_jawab,true);
		$criteria->compare('hubungan',$this->hubungan,true);
		$criteria->compare('alamat_penanggung_jawab',$this->alamat_penanggung_jawab,true);
		$criteria->compare('telepon_penanggung_jawab',$this->telepon_penanggung_jawab,true);
		$criteria->compare('kartu_identitas_penanggung_jawab',$this->kartu_identitas_penanggung_jawab,true);
		$criteria->compare('nomor_identitas_penanggung_jawab',$this->nomor_identitas_penanggung_jawab,true);
		$criteria->compare('no_ktp',$this->no_ktp);
		$criteria->compare('no_sim',$this->no_sim);
		$criteria->compare('no_pasport',$this->no_pasport);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getConcatened()
	{
	   return $this->id_pasien.'. '.$this->nama;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pasien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
