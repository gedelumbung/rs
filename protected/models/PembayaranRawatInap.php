<?php

/**
 * This is the model class for table "tbl_pembayaran_rawat_inap".
 *
 * The followings are the available columns in table 'tbl_pembayaran_rawat_inap':
 * @property integer $id_pembayaran_rawat_inap
 * @property integer $id_tr_tindakan_rawat_inap
 * @property string $tanggal
 * @property integer $jumlah
 * @property string $keterangan
 */
class PembayaranRawatInap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_pembayaran_rawat_inap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tr_tindakan_rawat_inap, tanggal, jumlah, keterangan', 'required'),
			array('id_tr_tindakan_rawat_inap, jumlah', 'numerical', 'integerOnly'=>true),
			array('tanggal', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pembayaran_rawat_inap, id_tr_tindakan_rawat_inap, tanggal, jumlah, keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pembayaran_rawat_inap' => 'Id Pembayaran Rawat Inap',
			'id_tr_tindakan_rawat_inap' => 'Id Tr Tindakan Rawat Inap',
			'tanggal' => 'Tanggal',
			'jumlah' => 'Jumlah',
			'keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pembayaran_rawat_inap',$this->id_pembayaran_rawat_inap);
		$criteria->compare('id_tr_tindakan_rawat_inap',$this->id_tr_tindakan_rawat_inap);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('jumlah',$this->jumlah);
		$criteria->compare('keterangan',$this->keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PembayaranRawatInap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
