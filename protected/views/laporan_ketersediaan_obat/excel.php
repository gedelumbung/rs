<?php 	
$nama_file = "Laporan Ketersediaan Obat.xls"; 	
header("Pragma: public"); 	header("Expires: 0"); 	
header("Cache-Control: must-revalidate, post-check=0,pre-check=0"); 	
header("Content-Type: application/force-download"); 	
header("Content-Type: application/octet-stream"); 	
header("Content-Type: application/download"); 	
header("Content-Disposition: attachment;filename=".$nama_file.""); 	
header("Content-Transfer-Encoding: binary "); ?>

<h1>Laporan Ketersediaan Obat</h1>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td colspan="10">Periode : <?php echo $_POST['bulan']; ?> - <?php echo $_POST['tahun']; ?></td>
	</tr>
	<tr>
		<td>Nama Obat</td>
		<td>Kemasan</td>
		<td>Harga</td>
		<td>Stok Awal</td>
		<td>Penerimaan</td>
		<td>Pemakaian</td>
		<td>Total</td>
		<td>Jumlah</td>
		<td>Keterangan</td>
		<td>Exp Date</td>
	</tr>
<?php

foreach ($model as $val) {
	echo "<tr>";
	echo "<td>".$val['nama_obat']."</td>";
	echo "<td>".$val['kemasan']."</td>";
	echo "<td>".$val['harga']."</td>";
	echo "<td>".$val['stok_awal']."</td>";
	echo "<td>".$val['penerimaan']."</td>";
	echo "<td>".$val['pemakaian_habis']."</td>";
	echo "<td>".($val['stok_akhir']-$val['pemakaian_habis'])."</td>";
	echo "<td>".$val['jumlah']."</td>";
	echo "<td>".$val['keterangan']."</td>";
	echo "<td>".$val['exp_date']."</td>";
	echo "</tr>";
}

?>
</table>