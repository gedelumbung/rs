
<h1>Laporan Ketersediaan Obat</h1>
<form method="post" action="<?php echo Yii::app()->baseUrl; ?>/laporan_ketersediaan_obat/excel">
        <select name="bulan">
                <option value=''>Pilih Bulan</option>
                <?php
                        for($i=1;$i<=12;$i++)
                        {
                                if($i<10)
                                {
                                        echo '<option value="0'.$i.'">0'.$i.'</option>';
                                }
                                else
                                {
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                        }
                ?>
        </select>
        <select name="tahun">
                <?php
                        for($i=2013;$i<=date('Y')+1;$i++)
                        {
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                ?>
        </select>
        <input type="submit" value="Export to Excel" class="button">
</form>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Nama Obat</td>
		<td>Kemasan</td>
		<td>Harga</td>
		<td>Stok Awal</td>
		<td>Penerimaan</td>
		<td>Pemakaian</td>
		<td>Total</td>
		<td>Jumlah</td>
		<td>Keterangan</td>
		<td>Exp Date</td>
	</tr>
<?php

foreach ($model as $val) {
	echo "<tr>";
	echo "<td>".$val['nama_obat']."</td>";
	echo "<td>".$val['kemasan']."</td>";
	echo "<td>".$val['harga']."</td>";
	echo "<td>".$val['stok_awal']."</td>";
	echo "<td>".$val['penerimaan']."</td>";
	echo "<td>".$val['pemakaian_habis']."</td>";
	echo "<td>".($val['stok_akhir']-$val['pemakaian_habis'])."</td>";
	echo "<td>".$val['jumlah']."</td>";
	echo "<td>".$val['keterangan']."</td>";
	echo "<td>".$val['exp_date']."</td>";
	echo "</tr>";
}

?>
</table>