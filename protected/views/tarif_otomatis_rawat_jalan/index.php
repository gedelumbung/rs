
<h1>Manage Tarif Otomatis</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tindakan-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tr_tindakan_rawat_jalan',
		'TipeRegistrasi.tipe_registrasi',
        array( 
        	'header'=>'Nama Pasien', 
        	'name'=>'nama_pasien', 
        	'value'=>'$data->Pasien->nama' 
        	),
		'tgl_masuk',
		'jam_masuk',
		'tgl_keluar',
		/*
		'id_dokter',
		'diagnosa',
		'id_obat',
		'jumlah_obat',
		'id_info_kamar',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{Lihat Tarif}',
			'buttons'=>array(
				'Lihat Tarif'=>array('url'=>'$this->grid->controller->createUrl("/tarif_otomatis_rawat_jalan/detail/$data->id_tr_tindakan_rawat_jalan")')
			),
		),
	),
)); ?>
