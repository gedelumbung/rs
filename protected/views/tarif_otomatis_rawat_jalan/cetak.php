<script type="text/javascript">
window.print();
</script>


<div style="text-align:center;">
<h3><?php echo Yii::app()->params['siteTitle']; ?></h3>
<h4><?php echo Yii::app()->params['siteType']; ?></h4>
<h5><?php echo Yii::app()->params['siteAddress']; ?></h5>
</div>
<hr>

<?php 
	
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_jalan',
		'Pasien.no_rm',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'tgl_masuk',
		'jam_masuk',
		'keterangan_tindakan',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
	),
)); ?>
<br>
<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Nama Obat</td>
		<td>Jumlah Obat</td>
		<td>Tanggal</td>
		<td>Keterangan</td>
		<td>Sub Total</td>
	</tr>
<?php
	$total = 0;
	foreach($model_detail as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Obat->nama_obat."</td>";
		echo "<td>".$val->jumlah_obat	."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "<td>".$val->keterangan	."</td>";
		echo "<td>".$val->Obat->harga * $val->jumlah_obat	."</td>";
		echo "</tr>";
		$total = $total+($val->Obat->harga * $val->jumlah_obat);
	}
	
?>
	<tr>
		<td colspan="4">Total Biaya Penggunaan Alat Kesehatan</td>
		<td><?php echo $total; ?></td>
	</tr>
</table>
<br>


<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Tindakan</td>
		<td>Harga</td>
		<td>Tanggal</td>
	</tr>
<?php
	$total_tindakan = 0;
	foreach($model_detail_tindakan as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Tindakan->tindakan."</td>";
		echo "<td>".$val->Tindakan->harga."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "</tr>";
		$total_tindakan = $total_tindakan+($val->Tindakan->harga);
	}
	
?>
	<tr>
		<td colspan="2">Total Biaya Tindakan</td>
		<td><?php echo $total_tindakan; ?></td>
	</tr>
	<tr>
		<td colspan="2">Total Keseluruhan Biaya</td>
		<td><?php echo number_format($total+$total_tindakan, 2, ',', '.'); ?></td>
	</tr>
</table>