<!doctype html>
<html>
<head>
	<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	

	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/css/bootstrap.min.css" />
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/css/bootstrap-responsive.min.css" />
	<!-- Theme CSS -->
	<!--[if !IE]> -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/css/style.css" />
	<!-- <![endif]-->
	<!--[if IE]>
	<link rel="stylesheet" href="css/style_ie.css">
	<![endif]-->

	<!-- jQuery -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/js/jquery.min.js" />
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/js/bootstrap.min.js" />

	<!-- Just for demonstration -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/js/demonstration.min.js" />
	<!-- Theme scripts -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/asset/js/application.min.js" />

</head>
<body class='login-body'>
	<div class="login-wrap">
		<h4>SISTEM MANAJEMEN RUMAH SAKIT</h4>
		<div class="login">
			
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'enableAjaxValidation'=>false,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
				'htmlOptions' => array("class"=>"form-horizontal"),
			)); ?>

			<?php echo $form->errorSummary($model); ?>
			<?php echo Yii::app()->user->getFlash('login'); ?>

			<div class="email">
				<?php echo $form->textField($model,'username', array("class" => "input-block-level", "placeholder" => "Username")); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>

			<div class="pw">
				<?php echo $form->passwordField($model,'password', array("class" => "input-block-level", "placeholder" => "Password")); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>

			<button type="submit" value="Sign In" class='button button-basic-darkblue btn-block'>Sign In</button>
			
			<?php $this->endWidget(); ?>
		</div>
		<h5><?php echo Yii::app()->params['siteCopyright']; ?></h5>
	</div>
</body>

</html>