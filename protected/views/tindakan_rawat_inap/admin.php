<?php
/* @var $this Tindakan_rawat_inapController */
/* @var $model TindakanRawatInap */

$this->breadcrumbs=array(
	'Tindakan Rawat Inaps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatInap', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tindakan-rawat-inap-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tindakan Rawat Inap</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tindakan-rawat-inap-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tr_tindakan_rawat_inap',
		'TipeRegistrasi.tipe_registrasi',
        array( 
        	'header'=>'Bagian', 
        	'value'=>'$data->User->status' 
        	),
        array( 
        	'header'=>'Nama Pasien', 
        	'name'=>'nama_pasien', 
        	'value'=>'$data->Pasien->nama' 
        	),
		'tgl_masuk',
		'jam_masuk',
		'status_pasien',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{Pindah}',
			'buttons'=>array(
				'Pindah'=>array('url'=>'$this->grid->controller->createUrl("/tindakan_rawat_inap/pindah/$data->id_tr_tindakan_rawat_inap")')
			),
		),
		/*
		'id_dokter',
		'diagnosa',
		'id_info_kamar',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
