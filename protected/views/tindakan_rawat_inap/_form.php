<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $model TindakanRawatJalan */
/* @var $form CActiveForm */
?>
<div class="row-fluid">
	<div class="form span6">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'tindakan-rawat-jalan-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>true,
	)); ?>

		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<?php echo $form->labelEx($model,'id_tipe_registrasi'); ?>
			<?php echo CHtml::dropDownList(
							'TindakanRawatInap[id_tipe_registrasi]',$model->id_tipe_registrasi,array(''=>'Semua') + CHtml::listData(TipeRegistrasi::model()->findAll(),'id_tipe_registrasi','tipe_registrasi')); ?>
			
			<?php echo $form->error($model,'id_tipe_registrasi'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'no_rm'); ?>

			<?php
				$this->widget('ext.chosen.Chosen',array(
				   'name' => 'TindakanRawatInap[id_pasien]', // input name
				   'value' => $model->id_pasien, // selection
				   'data' => array(''=>'Semua') + CHtml::listData(Pasien::model()->findAll(),'id_pasien','no_rm'),
				));
			?>

			<?php echo $form->error($model,'no_rm'); ?>
			<br>
			<br>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'tgl_masuk'); ?>
			<?php echo $form->dateField($model,'tgl_masuk',array('size'=>50,'maxlength'=>50)); ?>
			<?php echo $form->error($model,'tgl_masuk'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'jam_masuk'); ?>
			<?php echo $form->timeField($model,'jam_masuk',array('size'=>50,'maxlength'=>50)); ?>
			<?php echo $form->error($model,'jam_masuk'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'tgl_keluar'); ?>
			<?php echo $form->dateField($model,'tgl_keluar',array('size'=>50,'maxlength'=>50)); ?>
			<?php echo $form->error($model,'tgl_keluar'); ?>
		</div>

	</div><!-- form -->
	<div class="form span6">

		<div class="row">
			<?php echo $form->labelEx($model,'id_dokter'); ?>

			<?php
			echo Chosen::multiSelect('TindakanRawatInap[id_dokter]', $model->id_dokter,CHtml::listData(Dokter::model()->findAll(),'id_dokter','nama_dokter'));
			?>

			<?php echo $form->error($model,'id_dokter'); ?>
			<br>
			<br>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'id_penyakit'); ?>

			<?php
			echo Chosen::multiSelect('TindakanRawatInap[id_penyakit]', $model->id_penyakit,CHtml::listData(Penyakit::model()->findAll(),'id_penyakit','penyakit'));
			?>

			<?php echo $form->error($model,'id_penyakit'); ?>
			<br>
			<br>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'diagnosa_primer'); ?>
			<?php echo $form->textArea($model,'diagnosa_primer',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'diagnosa_primer'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'diagnosa_sekunder'); ?>
			<?php echo $form->textArea($model,'diagnosa_sekunder',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'diagnosa_sekunder'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'id_info_kamar'); ?>
			<?php
				$criteria = new CDbCriteria;
				$criteria->condition = "tersedia = 'Tersedia'";
			?>
			<?php echo CHtml::dropDownList(
							'TindakanRawatInap[id_info_kamar]',$model->id_info_kamar,array(''=>'Semua') + CHtml::listData(Kamar::model()->findAll($criteria),'id_info_kamar','nama_kamar')); ?>
			<?php echo $form->error($model,'id_info_kamar'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'status_pasien'); ?>
	      <?php echo $form->dropDownList($model,'status_pasien',array("Masih Dirawat"=>"Masih Dirawat","Sudah Keluar"=>"Sudah Keluar"),array('empty'=>'--- Pilih Status Pasien ---')); ?>
			<?php echo $form->error($model,'status_pasien'); ?>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		</div>

	<?php $this->endWidget(); ?>

	</div><!-- form -->
</div>