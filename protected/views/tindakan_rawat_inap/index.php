<?php
/* @var $this Tindakan_rawat_inapController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tindakan Rawat Inaps',
);

$this->menu=array(
	array('label'=>'Create TindakanRawatInap', 'url'=>array('create')),
	array('label'=>'Manage TindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Tindakan Rawat Inaps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
