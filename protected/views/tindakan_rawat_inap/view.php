<?php
/* @var $this Tindakan_rawat_inapController */
/* @var $model TindakanRawatInap */

$this->breadcrumbs=array(
	'Tindakan Rawat Inaps'=>array('index'),
	$model->id_tr_tindakan_rawat_inap,
);

$this->menu=array(
	array('label'=>'List TindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatInap', 'url'=>array('create')),
	array('label'=>'Update TindakanRawatInap', 'url'=>array('update', 'id'=>$model->id_tr_tindakan_rawat_inap)),
	array('label'=>'Delete TindakanRawatInap', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tr_tindakan_rawat_inap),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>View TindakanRawatInap #<?php echo $model->id_tr_tindakan_rawat_inap; ?></h1>


<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>'Operations',
	));
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(array('label'=>'Cetak', 'url'=>array('cetak', 'id'=>$model->id_tr_tindakan_rawat_inap))),
		'htmlOptions'=>array('class'=>'button'),
	));
	$this->endWidget();
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_inap',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'tgl_masuk',
		'jam_masuk',
		'tgl_keluar',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
		'Kamar.nama_kamar',
		'status_pasien',
	),
)); ?>
