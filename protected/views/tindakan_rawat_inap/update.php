<?php
/* @var $this Tindakan_rawat_inapController */
/* @var $model TindakanRawatInap */

$this->breadcrumbs=array(
	'Tindakan Rawat Inaps'=>array('index'),
	$model->id_tr_tindakan_rawat_inap=>array('view','id'=>$model->id_tr_tindakan_rawat_inap),
	'Update',
);

$this->menu=array(
	array('label'=>'List TindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatInap', 'url'=>array('create')),
	array('label'=>'View TindakanRawatInap', 'url'=>array('view', 'id'=>$model->id_tr_tindakan_rawat_inap)),
	array('label'=>'Manage TindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Update TindakanRawatInap <?php echo $model->id_tr_tindakan_rawat_inap; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>