<?php
/* @var $this Tindakan_rawat_inapController */
/* @var $model TindakanRawatInap */

$this->breadcrumbs=array(
	'Tindakan Rawat Inaps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Manage TindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Create TindakanRawatInap</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>