<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
                $model=LogDokterRawatInap::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= $value->Dokter->nama_dokter.', ';
                }
                return $val;
        }
?>

<?php $this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'pemakaian-alat-rawat-inap-grid',
	'dataProvider'=>$model->search_report_bulan(),
	'columns'=>array(
                'TindakanRawatInap.Pasien.no_rm',
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatInap->id_tr_tindakan_rawat_inap)',
                'type'=>'raw'
                ),
        array( 
                'header'=>'Registrasi', 
                'name'=>'id_tipe_registrasi', 
                'value'=>'$data->TindakanRawatInap->TipeRegistrasi->tipe_registrasi' 
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatInap->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatInap->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatInap->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'Obat.harga',
        array( 
        	'header'=>'Jumlah Total', 
        	'value'=>'$data->jumlah_obat * $data->Obat->harga' 
        	),
	),
)); ?>
