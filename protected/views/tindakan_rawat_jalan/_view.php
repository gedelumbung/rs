<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $data TindakanRawatJalan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_jalan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tr_tindakan_rawat_jalan), array('view', 'id'=>$data->id_tr_tindakan_rawat_jalan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_registrasi')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_registrasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pasien')); ?>:</b>
	<?php echo CHtml::encode($data->id_pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->jam_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dokter')); ?>:</b>
	<?php echo CHtml::encode($data->id_dokter); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('diagnosa')); ?>:</b>
	<?php echo CHtml::encode($data->diagnosa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_obat')); ?>:</b>
	<?php echo CHtml::encode($data->id_obat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_obat')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_obat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_info_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->id_info_kamar); ?>
	<br />

	*/ ?>

</div>