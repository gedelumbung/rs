<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tindakan Rawat Jalans',
);

$this->menu=array(
	array('label'=>'Create TindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'Manage TindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Tindakan Rawat Jalans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
