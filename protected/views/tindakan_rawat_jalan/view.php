<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $model TindakanRawatJalan */

$this->breadcrumbs=array(
	'Tindakan Rawat Jalans'=>array('index'),
	$model->id_tr_tindakan_rawat_jalan,
);

$this->menu=array(
	array('label'=>'List TindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'Update TindakanRawatJalan', 'url'=>array('update', 'id'=>$model->id_tr_tindakan_rawat_jalan)),
	array('label'=>'Delete TindakanRawatJalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tr_tindakan_rawat_jalan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>View TindakanRawatJalan #<?php echo $model->id_tr_tindakan_rawat_jalan; ?></h1>


<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>'Operations',
	));
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(array('label'=>'Cetak', 'url'=>array('cetak', 'id'=>$model->id_tr_tindakan_rawat_jalan))),
		'htmlOptions'=>array('class'=>'button'),
	));
	$this->endWidget();
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_jalan',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'tgl_masuk',
		'jam_masuk',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
	),
)); ?>
