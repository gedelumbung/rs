<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $model TindakanRawatJalan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tipe_registrasi'); ?>
			<?php echo CHtml::dropDownList(
					'TindakanRawatJalan[id_tipe_registrasi]',$model->id_tipe_registrasi,array(''=>'Semua') + CHtml::listData(TipeRegistrasi::model()->findAll(),'id_tipe_registrasi','tipe_registrasi')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_masuk'); ?>
		<?php echo $form->dateField($model,'tgl_masuk',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->