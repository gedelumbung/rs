<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $model TindakanRawatJalan */

$this->breadcrumbs=array(
	'Tindakan Rawat Jalans'=>array('index'),
	$model->id_tr_tindakan_rawat_jalan=>array('view','id'=>$model->id_tr_tindakan_rawat_jalan),
	'Update',
);

$this->menu=array(
	array('label'=>'List TindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'View TindakanRawatJalan', 'url'=>array('view', 'id'=>$model->id_tr_tindakan_rawat_jalan)),
	array('label'=>'Manage TindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Update Tindakan Rawat Jalan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>