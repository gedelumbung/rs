<?php
/* @var $this Tindakan_rawat_jalanController */
/* @var $model TindakanRawatJalan */

$this->breadcrumbs=array(
	'Tindakan Rawat Jalans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tindakan Rawat Jalan', 'url'=>array('index')),
	array('label'=>'Manage Tindakan Rawat Jalan', 'url'=>array('admin')),
);
?>

<h1>Create Tindakan Rawat Jalan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>