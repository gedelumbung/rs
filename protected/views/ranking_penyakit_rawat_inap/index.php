
<h1>Ranking Penyakit Pasien Rawat Inap</h1>
<form method="post" action="<?php echo Yii::app()->baseUrl; ?>/ranking_penyakit_rawat_inap/excel">
        <select name="bulan">
                <option value=''>Pilih Bulan</option>
                <?php
                        for($i=1;$i<=12;$i++)
                        {
                                if($i<10)
                                {
                                        echo '<option value="0'.$i.'">0'.$i.'</option>';
                                }
                                else
                                {
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                        }
                ?>
        </select>
        <select name="tahun">
                <?php
                        for($i=2013;$i<=date('Y')+1;$i++)
                        {
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                ?>
        </select>
        <input type="submit" value="Export to Excel" class="button">
</form>
<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>No</td>
		<td>Nama Penyakit</td>
		<td>Banyak Penyakit</td>
	</tr>
<?php
$no = 1;
foreach ($model as $val) {
	echo "<tr>";
	echo "<td>".$no."</td>";
	echo "<td>".$val['penyakit']."</td>";
	echo "<td>".$val['total']."</td>";
	echo "</tr>";
	$no++;
}

?>
</table>