<?php
/* @var $this ObatController */
/* @var $model Obat */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'obat-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_obat'); ?>
		<?php echo $form->textField($model,'nama_obat',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nama_obat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jenis_obat'); ?>
		<?php echo $form->textField($model,'jenis_obat',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'jenis_obat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php echo $form->textArea($model,'keterangan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kemasan'); ?>
		<?php echo $form->textField($model,'kemasan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'kemasan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga'); ?>
		<?php echo $form->textField($model,'harga'); ?>
		<?php echo $form->error($model,'harga'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stok_awal'); ?>
		<?php echo $form->textField($model,'stok_awal'); ?>
		<?php echo $form->error($model,'stok_awal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penerimaan'); ?>
		<?php echo $form->textField($model,'penerimaan'); ?>
		<?php echo $form->error($model,'penerimaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pemakaian'); ?>
		<?php echo $form->textField($model,'pemakaian'); ?>
		<?php echo $form->error($model,'pemakaian'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'exp_date'); ?>
		<?php echo $form->textField($model,'exp_date',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'exp_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->