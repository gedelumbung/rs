<?php
/* @var $this TarifController */
/* @var $data Tarif */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_info_tarif')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_info_tarif), array('view', 'id'=>$data->id_info_tarif)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_tarif')); ?>:</b>
	<?php echo CHtml::encode($data->nama_tarif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tarif')); ?>:</b>
	<?php echo CHtml::encode($data->tarif); ?>
	<br />


</div>