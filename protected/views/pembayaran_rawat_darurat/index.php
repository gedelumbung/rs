
<h1>Pembayaran Rawat Darurat</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tindakan-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tr_tindakan_rawat_darurat',
		'TipeRegistrasi.tipe_registrasi',
        array( 
        	'header'=>'Nama Pasien', 
        	'name'=>'nama_pasien', 
        	'value'=>'$data->Pasien->nama' 
        	),
		'tgl_masuk',
		'jam_masuk',
		/*
		'id_dokter',
		'diagnosa',
		'id_obat',
		'jumlah_obat',
		'id_info_kamar',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{Pembayaran}',
			'buttons'=>array(
				'Pembayaran'=>array('url'=>'$this->grid->controller->createUrl("/pembayaran_rawat_darurat/detail/$data->id_tr_tindakan_rawat_darurat")')
			),
		),
	),
)); ?>
