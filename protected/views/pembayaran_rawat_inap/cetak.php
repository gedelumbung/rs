<script type="text/javascript">
window.print();
</script>


<div style="text-align:center;">
<h3><?php echo Yii::app()->params['siteTitle']; ?></h3>
<h4><?php echo Yii::app()->params['siteType']; ?></h4>
<h5><?php echo Yii::app()->params['siteAddress']; ?></h5>
</div>
<hr>

<h1>Invoice Pembayaran Rawat Inap #<?php echo $model->id_tr_tindakan_rawat_inap; ?></h1>

<?php 
	$diff = date_diff(date_create($model->tgl_masuk),date_create(date('Y-m-d')));
	$lama = $diff->format("%a");
	if($lama==0){ $lama = 1; }
?>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>ID</td>
		<td>Tipe Registrasi</td>
		<td>Nama Pasien</td>
		<td>Tanggal Masuk</td>
		<td>Dokter</td>
		<td>Kamar</td>
		<td>Harga Kamar</td>
		<td>Lama</td>
		<td>Total</td>
	</tr>
<?php
	$total = 0;
		echo "<tr>";
		echo "<td>".$model['id_tr_tindakan_rawat_inap']."</td>";
		echo "<td>".$model['TipeRegistrasi']->tipe_registrasi	."</td>";
		echo "<td>".$model['Pasien']->nama	."</td>";
		echo "<td>".$model['tgl_masuk']	."</td>";
		echo "<td>".$nama_dokter."</td>";
		echo "<td>".$model['Kamar']->nama_kamar	."</td>";
		echo "<td>".$model['Kamar']->harga	."</td>";
		echo "<td>".$lama	." hari</td>";
		echo "<td>".number_format($lama*$model['Kamar']->harga, 2, ',', '.')	."</td>";
		echo "</tr>";
?>
</table>



<br>
<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Nama Obat</td>
		<td>Jumlah Obat</td>
		<td>Tanggal</td>
		<td>Keterangan</td>
		<td>Sub Total</td>
	</tr>
<?php
	$total = 0;
	foreach($model_detail as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Obat->nama_obat."</td>";
		echo "<td>".$val->jumlah_obat	."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "<td>".$val->keterangan	."</td>";
		echo "<td>".$val->Obat->harga * $val->jumlah_obat	."</td>";
		echo "</tr>";
		$total = $total+($val->Obat->harga * $val->jumlah_obat);
	}
	
?>
	<tr>
		<td colspan="4">Total Biaya Penggunaan Alat Kesehatan</td>
		<td><?php echo $total; ?></td>
	</tr>
</table>
<br>


<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Tindakan</td>
		<td>Harga</td>
		<td>Tanggal</td>
	</tr>
<?php
	$total_tindakan = 0;
	foreach($model_detail_tindakan as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Tindakan->tindakan."</td>";
		echo "<td>".$val->Tindakan->harga."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "</tr>";
		$total_tindakan = $total_tindakan+($val->Tindakan->harga);
	}
	$total_keseluruhan = $lama*$model->Kamar->harga+$total+$total_tindakan;
	
?>
	<tr>
		<td colspan="2">Total Biaya Tindakan</td>
		<td><?php echo $total_tindakan; ?></td>
	</tr>
	<tr>
		<td colspan="2">Total Keseluruhan Biaya</td>
		<td><?php echo number_format($lama*$model->Kamar->harga+$total+$total_tindakan, 2, ',', '.'); ?></td>
	</tr>
</table>

<h1>History Pembayaran Rawat Inap #<?php echo $model->id_tr_tindakan_rawat_inap; ?></h1>

<?php
$this->widget("ext.magnific-popup.EMagnificPopup", array(
'target' => '#pages-popup',
'type' => 'iframe',
));
?>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>No.</td>
		<td>Tanggal</td>
		<td>Jumlah</td>
		<td>Keterangan</td>
	</tr>
<?php
	$no = 1;
	$total_bayar = 0;
	foreach($model_pembayaran as $val_bayar)
	{
		echo "<tr>";
		echo "<td>".$no."</td>";
		echo "<td>".$val_bayar->tanggal	."</td>";
		echo "<td>".$val_bayar->jumlah	."</td>";
		echo "<td>".$val_bayar->keterangan	."</td>";
		echo "</tr>";
		$no++;
	$total_bayar = $total_bayar+$val_bayar->jumlah;
	}

	$status = "Belum Lunas";
	if($total_bayar>=$total_keseluruhan) { $status="Lunas"; }
	
?>
	<tr>
		<td colspan="3">Total Keseluruhan Pembayaran</td>
		<td><?php echo number_format($total_bayar, 2, ',', '.'); ?></td>
	</tr>
	<tr>
		<td colspan="3">Status Pembayaran</td>
		<td><?php echo $status; ?></td>
	</tr>
</table>