<?php
/* @var $this Tindakan_rawat_daruratController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tindakan Rawat Darurats',
);

$this->menu=array(
	array('label'=>'Create TindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'Manage TindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Tindakan Rawat Darurats</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
