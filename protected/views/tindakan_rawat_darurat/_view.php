<?php
/* @var $this Tindakan_rawat_daruratController */
/* @var $data TindakanRawatDarurat */
?>
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_darurat')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tr_tindakan_rawat_darurat), array('view', 'id'=>$data->id_tr_tindakan_rawat_darurat)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_registrasi')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_registrasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pasien')); ?>:</b>
	<?php echo CHtml::encode($data->id_pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jam_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->jam_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_dokter')); ?>:</b>
	<?php echo CHtml::encode($data->id_dokter); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('diagnosa')); ?>:</b>
	<?php echo CHtml::encode($data->diagnosa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_info_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->id_info_kamar); ?>
	<br />

	*/ ?>

</div>