<script type="text/javascript">
window.print();
</script>


<div style="text-align:center;">
<h3><?php echo Yii::app()->params['siteTitle']; ?></h3>
<h4><?php echo Yii::app()->params['siteType']; ?></h4>
<h5><?php echo Yii::app()->params['siteAddress']; ?></h5>
</div>
<hr>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_darurat',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'Pasien.alamat',
		array(
				'name' => 'bagian',
            'type'=>'raw',
				'value' => $model->User->status
			),
		array(
				'name' => 'Jenis Kelamin',
            'type'=>'raw',
				'value' => $model->Pasien->jk
			),
		array(
				'name' => 'Tanggal Lahir',
            'type'=>'raw',
				'value' => $model->Pasien->tgl_lahir
			),
		'tgl_masuk',
		'jam_masuk',
		'tgl_keluar',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
		'Kamar.nama_kamar',
		'status_pasien',
	),
)); ?>