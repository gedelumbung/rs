<?php
/* @var $this Tindakan_rawat_daruratController */
/* @var $model TindakanRawatDarurat */

$this->breadcrumbs=array(
	'Tindakan Rawat Darurats'=>array('index'),
	$model->id_tr_tindakan_rawat_darurat,
);

$this->menu=array(
	array('label'=>'List TindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'Update TindakanRawatDarurat', 'url'=>array('update', 'id'=>$model->id_tr_tindakan_rawat_darurat)),
	array('label'=>'Delete TindakanRawatDarurat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tr_tindakan_rawat_darurat),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>View TindakanRawatDarurat #<?php echo $model->id_tr_tindakan_rawat_darurat; ?></h1>


<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>'Operations',
	));
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(array('label'=>'Cetak', 'url'=>array('cetak', 'id'=>$model->id_tr_tindakan_rawat_darurat))),
		'htmlOptions'=>array('class'=>'button'),
	));
	$this->endWidget();
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_darurat',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'tgl_masuk',
		'jam_masuk',
		'tgl_keluar',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
		'Kamar.nama_kamar',
		'status_pasien',
	),
)); ?>
