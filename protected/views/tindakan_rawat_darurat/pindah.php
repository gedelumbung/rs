<?php
/* @var $this Tindakan_rawat_daruratController */
/* @var $model TindakanRawatDarurat */

$this->breadcrumbs=array(
	'Tindakan Rawat Darurats'=>array('index'),
	$model->id_tr_tindakan_rawat_darurat=>array('view','id'=>$model->id_tr_tindakan_rawat_darurat),
	'Update',
);

$this->menu=array(
	array('label'=>'List TindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create TindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'View TindakanRawatDarurat', 'url'=>array('view', 'id'=>$model->id_tr_tindakan_rawat_darurat)),
	array('label'=>'Manage TindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Pindah TindakanRawatDarurat <?php echo $model->id_tr_tindakan_rawat_darurat; ?></h1>

<?php $this->renderPartial('_formpindah', array('model'=>$model)); ?>