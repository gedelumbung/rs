<?php
/* @var $this Tindakan_rawat_daruratController */
/* @var $model TindakanRawatDarurat */

$this->breadcrumbs=array(
	'Tindakan Rawat Darurats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Manage TindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Create TindakanRawatDarurat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>