<h1>Invoice Pembayaran Rawat Jalan #<?php echo $model->id_tr_tindakan_rawat_jalan; ?></h1>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Nama Obat</td>
		<td>Jumlah Obat</td>
		<td>Tanggal</td>
		<td>Keterangan</td>
		<td>Sub Total</td>
	</tr>
<?php
	$total = 0;
	foreach($model_detail as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Obat->nama_obat."</td>";
		echo "<td>".$val->jumlah_obat	."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "<td>".$val->keterangan	."</td>";
		echo "<td>".$val->Obat->harga * $val->jumlah_obat	."</td>";
		echo "</tr>";
		$total = $total+($val->Obat->harga * $val->jumlah_obat);
	}
	
?>
	<tr>
		<td colspan="4">Total Biaya Penggunaan Alat Kesehatan</td>
		<td><?php echo $total; ?></td>
	</tr>
</table>
<br>


<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Tindakan</td>
		<td>Harga</td>
		<td>Tanggal</td>
	</tr>
<?php
	$total_tindakan = 0;
	foreach($model_detail_tindakan as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Tindakan->tindakan."</td>";
		echo "<td>".$val->Tindakan->harga."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "</tr>";
		$total_tindakan = $total_tindakan+($val->Tindakan->harga);
	}
	$total_keseluruhan = $total+$total_tindakan;
	
?>
	<tr>
		<td colspan="2">Total Biaya Tindakan</td>
		<td><?php echo $total_tindakan; ?></td>
	</tr>
	<tr>
		<td colspan="2">Total Keseluruhan Biaya</td>
		<td><?php echo number_format($total+$total_tindakan, 2, ',', '.'); ?></td>
	</tr>
</table>

<h1>History Pembayaran Rawat Jalan #<?php echo $model->id_tr_tindakan_rawat_jalan; ?></h1>

<?php
$this->widget("ext.magnific-popup.EMagnificPopup", array(
'target' => '#pages-popup',
'type' => 'iframe',
));
?>

<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>No.</td>
		<td>Tanggal</td>
		<td>Jumlah</td>
		<td>Keterangan</td>
		<td><a href="<?php echo Yii::app()->baseUrl; ?>/pembayaran_rawat_jalan/create/<?php echo $model->id_tr_tindakan_rawat_jalan; ?>" id="pages-popup" class="button">Tambah</a>
			<a href="<?php echo Yii::app()->baseUrl; ?>/pembayaran_rawat_jalan/cetak/<?php echo $model->id_tr_tindakan_rawat_jalan; ?>" class="button">Cetak</a>
		</td>
	</tr>
<?php
	$no = 1;
	$total_bayar = 0;
	foreach($model_pembayaran as $val_bayar)
	{
		echo "<tr>";
		echo "<td>".$no."</td>";
		echo "<td>".$val_bayar->tanggal	."</td>";
		echo "<td>".$val_bayar->jumlah	."</td>";
		echo "<td>".$val_bayar->keterangan	."</td>";
		echo "<td>
			<a href='".Yii::app()->baseUrl; ?>/pembayaran_rawat_jalan/delete/<?php echo $val_bayar->id_pembayaran_rawat_jalan."'>Hapus</a>
		</td>";
		echo "</tr>";
		$no++;
	$total_bayar = $total_bayar+$val_bayar->jumlah;
	}

	$status = "Belum Lunas";
	if($total_bayar>=$total_keseluruhan) { $status="Lunas"; }
	
?>
	<tr>
		<td colspan="4">Total Keseluruhan Pembayaran</td>
		<td><?php echo number_format($total_bayar, 2, ',', '.'); ?></td>
	</tr>
	<tr>
		<td colspan="4">Status Pembayaran</td>
		<td><?php echo $status; ?></td>
	</tr>
</table>