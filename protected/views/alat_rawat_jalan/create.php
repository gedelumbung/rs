<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Jalans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatJalan', 'url'=>array('index')),
	array('label'=>'Manage PemakaianAlatRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Create PemakaianAlatRawatJalan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>