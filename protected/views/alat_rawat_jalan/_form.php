<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pemakaian-alat-rawat-jalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_tr_tindakan_rawat_jalan'); ?>

			<?php
				$this->widget('ext.chosen.Chosen',array(
				   'name' => 'PemakaianAlatRawatJalan[id_tr_tindakan_rawat_jalan]', // input name
				   'value' => $model->id_tr_tindakan_rawat_jalan, // selection
				   'data' => array(''=>'Semua') + CHtml::listData(TindakanRawatJalan::model()->findAll(),'id_tr_tindakan_rawat_jalan','concatened'),
				));
			?>
			
		<?php echo $form->error($model,'id_tr_tindakan_rawat_jalan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_obat'); ?>
			<?php echo CHtml::dropDownList(
							'PemakaianAlatRawatJalan[id_obat]',$model->id_obat,array(''=>'Semua') + CHtml::listData(Obat::model()->findAll(),'id_obat','nama_obat')); ?>
		<?php echo $form->error($model,'id_obat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jumlah_obat'); ?>
		<?php echo $form->textField($model,'jumlah_obat'); ?>
		<?php echo $form->error($model,'jumlah_obat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->dateField($model,'tanggal',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php echo $form->textArea($model,'keterangan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->