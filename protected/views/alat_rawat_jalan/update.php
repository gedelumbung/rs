<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Jalans'=>array('index'),
	$model->id_pemakaian_alat_rawat_jalan=>array('view','id'=>$model->id_pemakaian_alat_rawat_jalan),
	'Update',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatJalan', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatJalan', 'url'=>array('create')),
	array('label'=>'View PemakaianAlatRawatJalan', 'url'=>array('view', 'id'=>$model->id_pemakaian_alat_rawat_jalan)),
	array('label'=>'Manage PemakaianAlatRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Update PemakaianAlatRawatJalan <?php echo $model->id_pemakaian_alat_rawat_jalan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>