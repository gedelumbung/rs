<?php
/* @var $this Alat_rawat_jalanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Jalans',
);

$this->menu=array(
	array('label'=>'Create PemakaianAlatRawatJalan', 'url'=>array('create')),
	array('label'=>'Manage PemakaianAlatRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Pemakaian Alat Rawat Jalans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
