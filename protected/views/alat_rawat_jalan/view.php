<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Jalans'=>array('index'),
	$model->id_pemakaian_alat_rawat_jalan,
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatJalan', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatJalan', 'url'=>array('create')),
	array('label'=>'Update PemakaianAlatRawatJalan', 'url'=>array('update', 'id'=>$model->id_pemakaian_alat_rawat_jalan)),
	array('label'=>'Delete PemakaianAlatRawatJalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pemakaian_alat_rawat_jalan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PemakaianAlatRawatJalan', 'url'=>array('admin')),
);
?>

<h1>View PemakaianAlatRawatJalan #<?php echo $model->id_pemakaian_alat_rawat_jalan; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pemakaian_alat_rawat_jalan',
		'TindakanRawatJalan.Pasien.nama',
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		'keterangan',
	),
)); ?>
