<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Jalan'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Pemakaian Alat Rawat Jalan', 'url'=>array('index')),
	array('label'=>'Create Pemakaian Alat Rawat Jalan', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pemakaian-alat-rawat-jalan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pemakaian Alat Rawat Jalan</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pemakaian-alat-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pemakaian_alat_rawat_jalan',
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatJalan->Pasien->nama' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		'keterangan',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
