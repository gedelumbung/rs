<?php
/* @var $this TindakanController */
/* @var $model Tindakan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'kategori'); ?>
	      <?php echo $form->dropDownList($model,'kategori',array(
			      "medis"=>"medis",
			      "penunjang"=>"penunjang",
		      ),
	      array('empty'=>'--- Pilih kategori ---')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tindakan'); ?>
		<?php echo $form->textField($model,'tindakan',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->