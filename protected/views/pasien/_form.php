<?php
/* @var $this PasienController */
/* @var $model Pasien */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pasien-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_alias'); ?>
		<?php echo $form->textField($model,'nama_alias',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nama_alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jk'); ?>
		<?php echo $form->textField($model,'jk',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'jk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tgl_lahir'); ?>
		<?php echo $form->textField($model,'tgl_lahir',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'tgl_lahir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nik_pasien'); ?>
		<?php echo $form->textField($model,'nik_pasien',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'nik_pasien'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alamat'); ?>
		<?php echo $form->textArea($model,'alamat',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'alamat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telpon_home'); ?>
		<?php echo $form->textField($model,'telpon_home',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'telpon_home'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telpon_mobile'); ?>
		<?php echo $form->textField($model,'telpon_mobile',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'telpon_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pekerjaan'); ?>
		<?php echo $form->textArea($model,'pekerjaan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'pekerjaan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'program_jaminan'); ?>
		<?php echo $form->textField($model,'program_jaminan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'program_jaminan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_sep_no_sktm'); ?>
		<?php echo $form->textField($model,'no_sep_no_sktm',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'no_sep_no_sktm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_kartu'); ?>
		<?php echo $form->textField($model,'no_kartu',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'no_kartu'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'asal_rujukan'); ?>
		<?php echo $form->textArea($model,'asal_rujukan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'asal_rujukan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->