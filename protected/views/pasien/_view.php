<?php
/* @var $this PasienController */
/* @var $data Pasien */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pasien')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pasien), array('view', 'id'=>$data->id_pasien)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_alias')); ?>:</b>
	<?php echo CHtml::encode($data->nama_alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jk')); ?>:</b>
	<?php echo CHtml::encode($data->jk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nik_pasien')); ?>:</b>
	<?php echo CHtml::encode($data->nik_pasien); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telpon_home')); ?>:</b>
	<?php echo CHtml::encode($data->telpon_home); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telpon_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->telpon_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pekerjaan')); ?>:</b>
	<?php echo CHtml::encode($data->pekerjaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('program_jaminan')); ?>:</b>
	<?php echo CHtml::encode($data->program_jaminan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_sep_no_sktm')); ?>:</b>
	<?php echo CHtml::encode($data->no_sep_no_sktm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_kartu')); ?>:</b>
	<?php echo CHtml::encode($data->no_kartu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('asal_rujukan')); ?>:</b>
	<?php echo CHtml::encode($data->asal_rujukan); ?>
	<br />

	*/ ?>

</div>