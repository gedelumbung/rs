<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
                $model=LogDokterRawatInap::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= $value->Dokter->nama_dokter.', ';
                }
                return $val;
        }
?>

<?php $this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'pemakaian-alat-rawat-jalan-grid',
	'dataProvider'=>$model->search_report(),
	'filter'=>$model,
	'columns'=>array(
		'tanggal',
        array( 
        	'header'=>'Tipe Registrasi', 
                'value'=>'$data->TindakanRawatInap->TipeRegistrasi->tipe_registrasi' 
        	),
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatInap->id_tr_tindakan_rawat_inap)',
                'type'=>'raw'
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatInap->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatInap->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatInap->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
	),
)); ?>