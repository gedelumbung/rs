<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inap'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Laporan Pemakaian Alat Rawat Inap', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pemakaian-alat-rawat-jalan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<form method="post" action="<?php echo Yii::app()->baseUrl; ?>/laporan_pemakaian_obat_rawat_inap/excel">
        <select name="bulan">
                <option value=''>Pilih Bulan</option>
                <?php
                        for($i=1;$i<=12;$i++)
                        {
                                if($i<10)
                                {
                                        echo '<option value="0'.$i.'">0'.$i.'</option>';
                                }
                                else
                                {
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                        }
                ?>
        </select>
        <select name="tahun">
                <?php
                        for($i=2013;$i<=date('Y')+1;$i++)
                        {
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                ?>
        </select>
        <input type="submit" value="Export to Excel" class="button">
</form>

<h1>Laporan Pemakaian Alat Rawat Inap</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
                $model=LogDokterRawatInap::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= '<li>'.$value->Dokter->nama_dokter.'</li>';
                }
                return $val;
        }
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pemakaian-alat-rawat-jalan-grid',
	'dataProvider'=>$model->search_report(),
	'filter'=>$model,
	'columns'=>array(
		'tanggal',
        array( 
        	'header'=>'Tipe Registrasi', 
                'value'=>'$data->TindakanRawatInap->TipeRegistrasi->tipe_registrasi' 
        	),
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatInap->id_tr_tindakan_rawat_inap)',
                'type'=>'raw'
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatInap->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatInap->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatInap->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
	),
)); ?>
