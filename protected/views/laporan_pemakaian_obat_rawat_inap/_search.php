<?php
/* @var $this Alat_rawat_jalanController */
/* @var $model PemakaianAlatRawatJalan */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tipe_registrasi'); ?>
			<?php echo CHtml::dropDownList(
							'PemakaianAlatRawatInap[id_tipe_registrasi]',$model->id_tipe_registrasi,array(''=>'Semua') + CHtml::listData(TipeRegistrasi::model()->findAll(),'id_tipe_registrasi','tipe_registrasi')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal'); ?>
		<?php echo $form->dateField($model,'tanggal',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->