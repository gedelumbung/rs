<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $model ManajamenTindakanRawatInap */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Inaps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatInap', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manajamen-tindakan-rawat-inap-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Manajamen Tindakan Rawat Inap</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manajamen-tindakan-rawat-inap-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tindakan_rawat_inap',
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatInap->Pasien->nama' 
        	),
		'Tindakan.tindakan',
		'tanggal',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
