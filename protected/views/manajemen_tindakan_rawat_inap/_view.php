<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $data ManajamenTindakanRawatInap */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan_rawat_inap')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan_rawat_inap), array('view', 'id'=>$data->id_tindakan_rawat_inap)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->id_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_inap')); ?>:</b>
	<?php echo CHtml::encode($data->id_tr_tindakan_rawat_inap); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />


</div>