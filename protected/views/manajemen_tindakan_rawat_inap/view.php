<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $model ManajamenTindakanRawatInap */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Inaps'=>array('index'),
	$model->id_tindakan_rawat_inap,
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatInap', 'url'=>array('create')),
	array('label'=>'Update ManajamenTindakanRawatInap', 'url'=>array('update', 'id'=>$model->id_tindakan_rawat_inap)),
	array('label'=>'Delete ManajamenTindakanRawatInap', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan_rawat_inap),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ManajamenTindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>View ManajamenTindakanRawatInap #<?php echo $model->id_tindakan_rawat_inap; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan_rawat_inap',
		'TindakanRawatInap.Pasien.nama',
		'Tindakan.tindakan',
		'tanggal',
	),
)); ?>
