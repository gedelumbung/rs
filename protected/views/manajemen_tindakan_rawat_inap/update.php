<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $model ManajamenTindakanRawatInap */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Inaps'=>array('index'),
	$model->id_tindakan_rawat_inap=>array('view','id'=>$model->id_tindakan_rawat_inap),
	'Update',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatInap', 'url'=>array('create')),
	array('label'=>'View ManajamenTindakanRawatInap', 'url'=>array('view', 'id'=>$model->id_tindakan_rawat_inap)),
	array('label'=>'Manage ManajamenTindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Update ManajamenTindakanRawatInap <?php echo $model->id_tindakan_rawat_inap; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>