<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $model ManajamenTindakanRawatInap */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Inaps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatInap', 'url'=>array('index')),
	array('label'=>'Manage ManajamenTindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Create ManajamenTindakanRawatInap</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>