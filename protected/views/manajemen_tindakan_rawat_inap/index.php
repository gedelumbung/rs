<?php
/* @var $this Manajemen_tindakan_rawat_inapController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Inaps',
);

$this->menu=array(
	array('label'=>'Create ManajamenTindakanRawatInap', 'url'=>array('create')),
	array('label'=>'Manage ManajamenTindakanRawatInap', 'url'=>array('admin')),
);
?>

<h1>Manajamen Tindakan Rawat Inaps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
