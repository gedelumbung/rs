<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_jalan = '".$id."'";
                $model=LogDokterRawatJalan::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= $value->Dokter->nama_dokter.', ';
                }
                return $val;
        }
?>

<?php $this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'pemakaian-alat-rawat-jalan-grid',
	'dataProvider'=>$model->search_report_bulan(),
	'columns'=>array(
                'TindakanRawatInap.Pasien.no_rm',
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatJalan->id_tr_tindakan_rawat_jalan)',
                'type'=>'raw'
                ),
        array( 
                'header'=>'Registrasi', 
                'name'=>'id_tipe_registrasi', 
                'value'=>'$data->TindakanRawatJalan->TipeRegistrasi->tipe_registrasi' 
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatJalan->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatJalan->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatJalan->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'Obat.harga',
        array( 
        	'header'=>'Jumlah Total', 
        	'value'=>'$data->jumlah_obat * $data->Obat->harga' 
        	),
	),
)); ?>
