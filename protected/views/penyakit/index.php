<?php
/* @var $this PenyakitController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Penyakits',
);

$this->menu=array(
	array('label'=>'Create Penyakit', 'url'=>array('create')),
	array('label'=>'Manage Penyakit', 'url'=>array('admin')),
);
?>

<h1>Penyakits</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
