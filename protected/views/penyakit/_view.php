<?php
/* @var $this PenyakitController */
/* @var $data Penyakit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_penyakit')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_penyakit), array('view', 'id'=>$data->id_penyakit)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penyakit')); ?>:</b>
	<?php echo CHtml::encode($data->penyakit); ?>
	<br />


</div>