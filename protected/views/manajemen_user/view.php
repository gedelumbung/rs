<?php
/* @var $this Manajemen_userController */
/* @var $model UserCmsModel */

$this->breadcrumbs=array(
	'User Cms Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserCmsModel', 'url'=>array('index')),
	array('label'=>'Create UserCmsModel', 'url'=>array('create')),
	array('label'=>'Update UserCmsModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserCmsModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserCmsModel', 'url'=>array('admin')),
);
?>

<h1>View UserCmsModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'nama',
		'email',
		'status',
	),
)); ?>
