<?php
/* @var $this Manajemen_userController */
/* @var $model UserCmsModel */

$this->breadcrumbs=array(
	'User Cms Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UserCms', 'url'=>array('index')),
	array('label'=>'Create UserCms', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-cms-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage User Cms</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-cms-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'nama',
		'email',
		'status',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
