<?php
/* @var $this Manajemen_userController */
/* @var $model UserCmsModel */

$this->breadcrumbs=array(
	'User Cms Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserCmsModel', 'url'=>array('index')),
	array('label'=>'Create UserCmsModel', 'url'=>array('create')),
	array('label'=>'View UserCmsModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserCmsModel', 'url'=>array('admin')),
);
?>

<h1>Update UserCmsModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>