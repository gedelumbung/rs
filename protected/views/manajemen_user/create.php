<?php
/* @var $this Manajemen_userController */
/* @var $model UserCmsModel */

$this->breadcrumbs=array(
	'User Cms Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserCmsModel', 'url'=>array('index')),
	array('label'=>'Manage UserCmsModel', 'url'=>array('admin')),
);
?>

<h1>Create UserCmsModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>