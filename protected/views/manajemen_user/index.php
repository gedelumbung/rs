<?php
/* @var $this Manajemen_userController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Cms Models',
);

$this->menu=array(
	array('label'=>'Create UserCmsModel', 'url'=>array('create')),
	array('label'=>'Manage UserCmsModel', 'url'=>array('admin')),
);
?>

<h1>User Cms Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
