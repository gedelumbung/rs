<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
                $model=LogDokterRawatDarurat::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= $value->Dokter->nama_dokter.', ';
                }
                return $val;
        }
?>

<?php $this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'pemakaian-alat-rawat-jalan-grid',
	'dataProvider'=>$model->search_report(),
	'filter'=>$model,
	'columns'=>array(
		'tanggal',
        array( 
                'header'=>'Tipe Registrasi', 
                'value'=>'$data->TindakanRawatDarurat->TipeRegistrasi->tipe_registrasi' 
        	),
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatDarurat->id_tr_tindakan_rawat_darurat)',
                'type'=>'raw'
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
	),
)); ?>
