<?php 	
$nama_file = "Laporan Ranking Penyakit Rawat Darurat.xls"; 	
header("Pragma: public"); 	header("Expires: 0"); 	
header("Cache-Control: must-revalidate, post-check=0,pre-check=0"); 	
header("Content-Type: application/force-download"); 	
header("Content-Type: application/octet-stream"); 	
header("Content-Type: application/download"); 	
header("Content-Disposition: attachment;filename=".$nama_file.""); 	
header("Content-Transfer-Encoding: binary "); ?>

<h1>Ranking Penyakit Pasien Rawat Darurat</h1>
<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td colspan="3">Periode : <?php echo $_POST['bulan']; ?> - <?php echo $_POST['tahun']; ?></td>
	</tr>
	<tr>
		<td>No</td>
		<td>Nama Penyakit</td>
		<td>Banyak Penyakit</td>
	</tr>
<?php
$no = 1;
foreach ($model as $val) {
	echo "<tr>";
	echo "<td>".$no."</td>";
	echo "<td>".$val['penyakit']."</td>";
	echo "<td>".$val['total']."</td>";
	echo "</tr>";
	$no++;
}

?>
</table>