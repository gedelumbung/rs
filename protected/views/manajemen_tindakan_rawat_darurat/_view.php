<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $data ManajamenTindakanRawatDarurat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan_rawat_darurat')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan_rawat_darurat), array('view', 'id'=>$data->id_tindakan_rawat_darurat)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan')); ?>:</b>
	<?php echo CHtml::encode($data->id_tindakan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_darurat')); ?>:</b>
	<?php echo CHtml::encode($data->id_tr_tindakan_rawat_darurat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />


</div>