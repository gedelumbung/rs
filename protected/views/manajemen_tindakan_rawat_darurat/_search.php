<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $model ManajamenTindakanRawatDarurat */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_tindakan_rawat_darurat'); ?>
		<?php echo $form->textField($model,'id_tindakan_rawat_darurat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_tindakan'); ?>
		<?php echo $form->textField($model,'id_tindakan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_tr_tindakan_rawat_darurat'); ?>
		<?php echo $form->textField($model,'id_tr_tindakan_rawat_darurat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tanggal'); ?>
		<?php echo $form->textField($model,'tanggal',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->