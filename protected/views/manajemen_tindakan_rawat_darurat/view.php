<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $model ManajamenTindakanRawatDarurat */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Darurats'=>array('index'),
	$model->id_tindakan_rawat_darurat,
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'Update ManajamenTindakanRawatDarurat', 'url'=>array('update', 'id'=>$model->id_tindakan_rawat_darurat)),
	array('label'=>'Delete ManajamenTindakanRawatDarurat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan_rawat_darurat),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ManajamenTindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>View ManajamenTindakanRawatDarurat #<?php echo $model->id_tindakan_rawat_darurat; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan_rawat_darurat',
		'TindakanRawatDarurat.Pasien.nama',
		'Tindakan.tindakan',
		'tanggal',
	),
)); ?>
