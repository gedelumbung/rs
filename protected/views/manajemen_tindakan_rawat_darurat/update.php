<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $model ManajamenTindakanRawatDarurat */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Darurats'=>array('index'),
	$model->id_tindakan_rawat_darurat=>array('view','id'=>$model->id_tindakan_rawat_darurat),
	'Update',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'View ManajamenTindakanRawatDarurat', 'url'=>array('view', 'id'=>$model->id_tindakan_rawat_darurat)),
	array('label'=>'Manage ManajamenTindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Update ManajamenTindakanRawatDarurat <?php echo $model->id_tindakan_rawat_darurat; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>