<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $model ManajamenTindakanRawatDarurat */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Darurats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatDarurat', 'url'=>array('index')),
	array('label'=>'Manage ManajamenTindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Create ManajamenTindakanRawatDarurat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>