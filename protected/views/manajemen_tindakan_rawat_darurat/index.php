<?php
/* @var $this Manajemen_tindakan_rawat_daruratController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Darurats',
);

$this->menu=array(
	array('label'=>'Create ManajamenTindakanRawatDarurat', 'url'=>array('create')),
	array('label'=>'Manage ManajamenTindakanRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Manajamen Tindakan Rawat Darurats</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
