<?php
/* @var $this KamarController */
/* @var $data Kamar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_info_kamar')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_info_kamar), array('view', 'id'=>$data->id_info_kamar)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kamar')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kamar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe')); ?>:</b>
	<?php echo CHtml::encode($data->tipe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga')); ?>:</b>
	<?php echo CHtml::encode($data->harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />


</div>