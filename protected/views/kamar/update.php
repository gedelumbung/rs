<?php
/* @var $this KamarController */
/* @var $model Kamar */

$this->breadcrumbs=array(
	'Kamars'=>array('index'),
	$model->id_info_kamar=>array('view','id'=>$model->id_info_kamar),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kamar', 'url'=>array('index')),
	array('label'=>'Create Kamar', 'url'=>array('create')),
	array('label'=>'View Kamar', 'url'=>array('view', 'id'=>$model->id_info_kamar)),
	array('label'=>'Manage Kamar', 'url'=>array('admin')),
);
?>

<h1>Update Kamar <?php echo $model->id_info_kamar; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>