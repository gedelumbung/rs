<script type="text/javascript">
window.print();
</script>


<div style="text-align:center;">
<h3><?php echo Yii::app()->params['siteTitle']; ?></h3>
<h4><?php echo Yii::app()->params['siteType']; ?></h4>
<h5><?php echo Yii::app()->params['siteAddress']; ?></h5>
</div>
<hr>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pasien',
		'no_rm',
		'nama',
		'nama_keluarga',
		'tempat_lahir',
		'tgl_lahir',
		'jk',
		'alamat',
		'kelurahan',
		'kode_pos',
		'provinsi',
		'telepon_rumah',
		'telepon_hp',
		'nama_perusahaan',
		'alamat_perusahaan',
		'kantor',
		'ext',
		'agama',
		'status',
		'pendidikan',
		'warga_negara',
		'penanggung_jawab',
		'nama_penanggung_jawab',
		'hubungan',
		'alamat_penanggung_jawab',
		'telepon_penanggung_jawab',
		'kartu_identitas_penanggung_jawab',
		'nomor_identitas_penanggung_jawab',
		'no_ktp',
		'no_sim',
		'no_pasport',
	),
)); ?>