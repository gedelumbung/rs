<script type="text/javascript">
window.print();
</script>

<style type="text/css">
h4{
	font-size: 14px;
	line-height: 0px;
	text-align: center;
}
h5{
	font-size: 13px;
	line-height: 0px;
	text-align: center;
}
h6{
	font-size: 12px;
	line-height: 15px;
	text-align: center;
}
h3{
	font-size: 16px;
	line-height: 15px;
	font-weight: bold;
	text-transform: uppercase;
}
hr{
	margin: 0px;
	border-top: 1px solid #000;
}
</style>

<table style="border:1px solid #000; margin:0px auto;" cellpadding="0" width="400px">
	<tr>
		<td colspan="3"><h4><?php echo Yii::app()->params['siteTitle']; ?></h4></td>
	</tr>
	<tr>
		<td colspan="3"><h5><?php echo Yii::app()->params['siteType']; ?></h5></td>
	</tr>
	<tr>
		<td colspan="3"><h6><?php echo Yii::app()->params['siteAddress']; ?></h6></td>
	</tr>
	<tr>
		<td colspan="3"><hr></td>
	</tr>
	<tr>
		<td colspan="3"><hr></td>
	</tr>
	<tr>
		<td colspan="3"><hr></td>
	</tr>
	<tr>
		<td style="padding:10px;" width="90px">
			No. RM
		</td>

		<td style="padding:10px;" width="10px">
			:
		</td>
		<td>
			<h3><?php echo $model->no_rm; ?></h3>
		</td>
	</tr>
	<tr>
		<td colspan="3"><h3 style="text-align:center"><?php echo $model->nama; ?></h3></td>
	</tr>
	<tr>
		<td style="padding:10px;" width="90px">
			Tanggal Lahir
		</td>

		<td style="padding:10px;" width="10px">
			:
		</td>
		<td>
			<h4 style="text-align:left"><?php echo date_format(date_create($model->tgl_lahir),'d-m-Y'); ?></h4>
		</td>
	</tr>
	<tr>
		<td style="padding:10px;" width="90px">
			Tanggal Daftar
		</td>

		<td style="padding:10px;" width="10px">
			:
		</td>
		<td>
			<h4 style="text-align:left"><?php echo date_format(date_create($model->tgl_daftar),'d-m-Y'); ?></h4>
		</td>
	</tr>
</table>
