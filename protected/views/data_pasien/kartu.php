<script type="text/javascript">
window.print();
</script>
<style type="text/css">
table.detail-view th {
	text-align: left;
}
table.diagnosa td {
	padding: 20px;
}
</style>

<div style="text-align:center;">
<h3><?php echo Yii::app()->params['siteTitle']; ?></h3>
<h4><?php echo Yii::app()->params['siteType']; ?></h4>
<h5><?php echo Yii::app()->params['siteAddress']; ?></h5>
</div>
<hr>

<div style="text-align:center;">
<h5>KARTU BEROBAT JALAN</h5>
</div>

<div style="width:45%; padding:10px 10px 30px 10px; float:left;">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'no_rm',
		'nama',
		'nama_keluarga',
		'tempat_lahir',
	),
)); ?>
</div>
<div style="width:45%; padding:10px; float:left;">
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tgl_lahir',
		'jk',
		'alamat',
		'telepon_rumah',
	),
)); ?>
</div>

<br>
<br>
<br>

<table border="1" cellpadding="8" width="100%" class="diagnosa">
	<thead>
		<th width="50px">No.</th>
		<th width="100px">Tanggal</th>
		<th>Keluhan</th>
		<th>Diagnosa</th>
		<th width="50px">Paraf</th>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
