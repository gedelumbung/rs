<?php
/* @var $this Data_pasienController */
/* @var $data Pasien */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pasien')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pasien), array('view', 'id'=>$data->id_pasien)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_keluarga')); ?>:</b>
	<?php echo CHtml::encode($data->nama_keluarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jk')); ?>:</b>
	<?php echo CHtml::encode($data->jk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kelurahan')); ?>:</b>
	<?php echo CHtml::encode($data->kelurahan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kode_pos')); ?>:</b>
	<?php echo CHtml::encode($data->kode_pos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provinsi')); ?>:</b>
	<?php echo CHtml::encode($data->provinsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_rumah')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_rumah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_hp')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_hp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kantor')); ?>:</b>
	<?php echo CHtml::encode($data->kantor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ext')); ?>:</b>
	<?php echo CHtml::encode($data->ext); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('warga_negara')); ?>:</b>
	<?php echo CHtml::encode($data->warga_negara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->nama_penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hubungan')); ?>:</b>
	<?php echo CHtml::encode($data->hubungan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon_penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->telepon_penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kartu_identitas_penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->kartu_identitas_penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor_identitas_penanggung_jawab')); ?>:</b>
	<?php echo CHtml::encode($data->nomor_identitas_penanggung_jawab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_ktp')); ?>:</b>
	<?php echo CHtml::encode($data->no_ktp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_sim')); ?>:</b>
	<?php echo CHtml::encode($data->no_sim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_pasport')); ?>:</b>
	<?php echo CHtml::encode($data->no_pasport); ?>
	<br />

	*/ ?>

</div>