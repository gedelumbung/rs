<?php
/* @var $this Data_pasienController */
/* @var $model Pasien */
/* @var $form CActiveForm */
?>

<div class="row-fluid">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'pasien-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>
	<div class="form span6">

			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<div class="row">
				<?php echo $form->labelEx($model,'no_rm'); ?>
				<?php echo $form->textField($model,'no_rm', ['readonly' => true]); ?>
				<?php echo $form->error($model,'no_rm'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'no_ktp'); ?>
				<?php echo $form->textField($model,'no_ktp'); ?>
				<?php echo $form->error($model,'no_ktp'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'no_sim'); ?>
				<?php echo $form->textField($model,'no_sim'); ?>
				<?php echo $form->error($model,'no_sim'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'no_pasport'); ?>
				<?php echo $form->textField($model,'no_pasport'); ?>
				<?php echo $form->error($model,'no_pasport'); ?>
			</div>


			<div class="row">
				<?php echo $form->labelEx($model,'nama'); ?>
				<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'nama'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'nama_keluarga'); ?>
				<?php echo $form->textField($model,'nama_keluarga',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'nama_keluarga'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'tempat_lahir'); ?>
				<?php echo $form->textField($model,'tempat_lahir',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'tempat_lahir'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'tgl_lahir'); ?>
				<?php echo $form->dateField($model,'tgl_lahir',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'tgl_lahir'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'jk'); ?>
		      <?php echo $form->dropDownList($model,'jk',array(
				      "Laki-Laki"=>"Laki-Laki",
				      "Perempuan"=>"Perempuan",
			      ),
		      array('empty'=>'--- Pilih Jenis Kelamin ---')); ?>
				<?php echo $form->error($model,'jk'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'alamat'); ?>
				<?php echo $form->textArea($model,'alamat',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'alamat'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'kelurahan'); ?>
				<?php echo $form->textField($model,'kelurahan',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'kelurahan'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'kode_pos'); ?>
				<?php echo $form->textField($model,'kode_pos',array('size'=>20,'maxlength'=>20)); ?>
				<?php echo $form->error($model,'kode_pos'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'provinsi'); ?>
				<?php echo $form->textField($model,'provinsi',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'provinsi'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'telepon_rumah'); ?>
				<?php echo $form->textField($model,'telepon_rumah',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'telepon_rumah'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'telepon_hp'); ?>
				<?php echo $form->textField($model,'telepon_hp',array('size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'telepon_hp'); ?>
			</div>

	</div>

	<div class="form span6">

			<div class="row">
				<?php echo $form->labelEx($model,'nama_perusahaan'); ?>
				<?php echo $form->textField($model,'nama_perusahaan',array('size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'nama_perusahaan'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'alamat_perusahaan'); ?>
				<?php echo $form->textArea($model,'alamat_perusahaan',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'alamat_perusahaan'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'kantor'); ?>
				<?php echo $form->textField($model,'kantor',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'kantor'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'ext'); ?>
				<?php echo $form->textField($model,'ext',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'ext'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'agama'); ?>
		      <?php echo $form->dropDownList($model,'agama',array(
				      "Islam"=>"Islam",
				      "Hindu"=>"Hindu",
				      "Budha"=>"Budha",
				      "Kristen Katholik"=>"Kristen Katholik",
				      "Kristen Protestan"=>"Kristen Protestan",
				      "Lainnya"=>"Lainnya",
			      ),
		      array('empty'=>'--- Pilih Agama ---')); ?>
				<?php echo $form->error($model,'agama'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
		      <?php echo $form->dropDownList($model,'status',array(
				      "Menikah"=>"Menikah",
				      "Belum/Tidak Menikah"=>"Belum/Tidak Menikah",
				      "Cerai"=>"Cerai",
			      ),
		      array('empty'=>'--- Pilih Status ---')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'pendidikan'); ?>
		      <?php echo $form->dropDownList($model,'pendidikan',array(
				      "SMA"=>"SMA",
				      "Akademik"=>"Akademik",
				      "Sarjana"=>"Sarjana",
				      "Lainnya"=>"Lainnya",
			      ),
		      array('empty'=>'--- Pilih Pendidikan ---')); ?>
				<?php echo $form->error($model,'pendidikan'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'warga_negara'); ?>
		      <?php echo $form->dropDownList($model,'warga_negara',array(
				      "WNI"=>"WNI",
				      "WNA"=>"WNA",
			      ),
		      array('empty'=>'--- Pilih Pendidikan ---')); ?>
				<?php echo $form->error($model,'warga_negara'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'penanggung_jawab'); ?>
		      <?php echo $form->dropDownList($model,'penanggung_jawab',array(
				      "Pribadi"=>"Pribadi",
				      "Perusahaan"=>"Perusahaan",
				      "Asuransi"=>"Asuransi",
			      ),
		      array('empty'=>'--- Pilih Pendidikan ---')); ?>
				<?php echo $form->error($model,'penanggung_jawab'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'nama_penanggung_jawab'); ?>
				<?php echo $form->textField($model,'nama_penanggung_jawab',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'nama_penanggung_jawab'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'hubungan'); ?>
				<?php echo $form->textField($model,'hubungan',array('size'=>60,'maxlength'=>150)); ?>
				<?php echo $form->error($model,'hubungan'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'alamat_penanggung_jawab'); ?>
				<?php echo $form->textArea($model,'alamat_penanggung_jawab',array('rows'=>6, 'cols'=>50)); ?>
				<?php echo $form->error($model,'alamat_penanggung_jawab'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'telepon_penanggung_jawab'); ?>
				<?php echo $form->textField($model,'telepon_penanggung_jawab',array('size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'telepon_penanggung_jawab'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'kartu_identitas_penanggung_jawab'); ?>
		      <?php echo $form->dropDownList($model,'kartu_identitas_penanggung_jawab',array(
				      "KTP"=>"KTP",
				      "SIM"=>"SIM",
				      "Paspor"=>"Paspor",
				      "Lainnya"=>"Lainnya",
			      ),
		      array('empty'=>'--- Pilih Kartu Identitas ---')); ?>
				<?php echo $form->error($model,'kartu_identitas_penanggung_jawab'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'nomor_identitas_penanggung_jawab'); ?>
				<?php echo $form->textField($model,'nomor_identitas_penanggung_jawab',array('size'=>60,'maxlength'=>100)); ?>
				<?php echo $form->error($model,'nomor_identitas_penanggung_jawab'); ?>
			</div>

			<div class="row buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			</div>
	</div>

<?php $this->endWidget(); ?>


</div><!-- form -->