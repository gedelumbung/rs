<?php
/* @var $this Data_pasienController */
/* @var $model Pasien */

$this->breadcrumbs=array(
	'Pasiens'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Pasien', 'url'=>array('index')),
	array('label'=>'Create Pasien', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pasien-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pasiens</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pasien-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'no_rm',
		'nama',
		'nama_keluarga',
		'tempat_lahir',
		'tgl_lahir',
		'jk',
		/*
		'alamat',
		'kelurahan',
		'kode_pos',
		'provinsi',
		'telepon_rumah',
		'telepon_hp',
		'nama_perusahaan',
		'alamat_perusahaan',
		'kantor',
		'ext',
		'agama',
		'status',
		'pendidikan',
		'warga_negara',
		'penanggung_jawab',
		'nama_penanggung_jawab',
		'hubungan',
		'alamat_penanggung_jawab',
		'telepon_penanggung_jawab',
		'kartu_identitas_penanggung_jawab',
		'nomor_identitas_penanggung_jawab',
		'no_ktp',
		'no_sim',
		'no_pasport',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
