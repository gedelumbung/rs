<?php
/* @var $this Data_pasienController */
/* @var $model Pasien */

$this->breadcrumbs=array(
	'Pasiens'=>array('index'),
	$model->id_pasien,
);

$this->menu=array(
	array('label'=>'List Pasien', 'url'=>array('index')),
	array('label'=>'Create Pasien', 'url'=>array('create')),
	array('label'=>'Update Pasien', 'url'=>array('update', 'id'=>$model->id_pasien)),
	array('label'=>'Delete Pasien', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pasien),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Pasien', 'url'=>array('admin')),
);
?>
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>'Operations',
	));
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(
			array('label'=>'Cetak Data Pasien', 'url'=>array('cetak', 'id'=>$model->id_pasien)),
			array('label'=>'Cetak Kartu Pasien', 'url'=>array('kartu_pasien', 'id'=>$model->id_pasien)),
			array('label'=>'Cetak Kartu Berobat', 'url'=>array('kartu', 'id'=>$model->id_pasien)),
		),
		'htmlOptions'=>array('class'=>'button'),
	));
	$this->endWidget();
?>

<h1>View Pasien #<?php echo $model->id_pasien; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pasien',
		'no_rm',
		'nama',
		'nama_keluarga',
		'tempat_lahir',
		'tgl_lahir',
		'jk',
		'alamat',
		'kelurahan',
		'kode_pos',
		'provinsi',
		'telepon_rumah',
		'telepon_hp',
		'nama_perusahaan',
		'alamat_perusahaan',
		'kantor',
		'ext',
		'agama',
		'status',
		'pendidikan',
		'warga_negara',
		'penanggung_jawab',
		'nama_penanggung_jawab',
		'hubungan',
		'alamat_penanggung_jawab',
		'telepon_penanggung_jawab',
		'kartu_identitas_penanggung_jawab',
		'nomor_identitas_penanggung_jawab',
		'no_ktp',
		'no_sim',
		'no_pasport',
	),
)); ?>
