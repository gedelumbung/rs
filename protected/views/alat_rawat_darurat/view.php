<?php
/* @var $this Alat_rawat_daruratController */
/* @var $model PemakaianAlatRawatDarurat */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Darurats'=>array('index'),
	$model->id_pemakaian_alat_rawat_darurat,
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatDarurat', 'url'=>array('create')),
	array('label'=>'Update PemakaianAlatRawatDarurat', 'url'=>array('update', 'id'=>$model->id_pemakaian_alat_rawat_darurat)),
	array('label'=>'Delete PemakaianAlatRawatDarurat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pemakaian_alat_rawat_darurat),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PemakaianAlatRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>View PemakaianAlatRawatDarurat #<?php echo $model->id_pemakaian_alat_rawat_darurat; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pemakaian_alat_rawat_darurat',
		'TindakanRawatDarurat.Pasien.nama',
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		'keterangan',
	),
)); ?>
