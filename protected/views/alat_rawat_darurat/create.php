<?php
/* @var $this Alat_rawat_daruratController */
/* @var $model PemakaianAlatRawatDarurat */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Darurats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatDarurat', 'url'=>array('index')),
	array('label'=>'Manage PemakaianAlatRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Create PemakaianAlatRawatDarurat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>