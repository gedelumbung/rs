<?php
/* @var $this Alat_rawat_daruratController */
/* @var $model PemakaianAlatRawatDarurat */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Darurats'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatDarurat', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pemakaian-alat-rawat-darurat-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pemakaian Alat Rawat Darurat</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pemakaian-alat-rawat-darurat-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pemakaian_alat_rawat_darurat',
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->nama' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
