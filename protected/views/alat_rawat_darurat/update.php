<?php
/* @var $this Alat_rawat_daruratController */
/* @var $model PemakaianAlatRawatDarurat */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Darurats'=>array('index'),
	$model->id_pemakaian_alat_rawat_darurat=>array('view','id'=>$model->id_pemakaian_alat_rawat_darurat),
	'Update',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatDarurat', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatDarurat', 'url'=>array('create')),
	array('label'=>'View PemakaianAlatRawatDarurat', 'url'=>array('view', 'id'=>$model->id_pemakaian_alat_rawat_darurat)),
	array('label'=>'Manage PemakaianAlatRawatDarurat', 'url'=>array('admin')),
);
?>

<h1>Update PemakaianAlatRawatDarurat <?php echo $model->id_pemakaian_alat_rawat_darurat; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>