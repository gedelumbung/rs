

<h1>View Tarif Otomatis #<?php echo $model->id_tr_tindakan_rawat_inap; ?></h1>
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>'Operations',
	));
	$this->widget('zii.widgets.CMenu', array(
		'items'=>array(array('label'=>'Cetak', 'url'=>array('cetak', 'id'=>$model->id_tr_tindakan_rawat_inap))),
		'htmlOptions'=>array('class'=>'button'),
	));
	$this->endWidget();
?>

<?php 
	$diff = date_diff(date_create($model->tgl_masuk),date_create($model->tgl_keluar));
	$lama = $diff->format("%a");
	if($lama==0){ $lama = 1; }
	
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tr_tindakan_rawat_inap',
		'Pasien.no_rm',
		'TipeRegistrasi.tipe_registrasi',
		'Pasien.nama',
		'tgl_masuk',
		'jam_masuk',
		'tgl_keluar',
		array(
				'name' => 'id_dokter',
            'type'=>'raw',
				'value' => $nama_dokter
			),
		array(
				'name' => 'id_penyakit',
            'type'=>'raw',
				'value' => $penyakit
			),
		'diagnosa_primer',
		'diagnosa_sekunder',
		'Kamar.nama_kamar',
		'Kamar.harga',
		array(
			'label' => 'Lama',
			'value' => $lama
			),
		array(
			'label' => 'Total Tarif Kamar',
			'value' => number_format($lama*$model->Kamar->harga, 2, ',', '.')
			)
	),
)); ?>
<br>
<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Nama Obat</td>
		<td>Jumlah Obat</td>
		<td>Tanggal</td>
		<td>Keterangan</td>
		<td>Sub Total</td>
	</tr>
<?php
	$total = 0;
	foreach($model_detail as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Obat->nama_obat."</td>";
		echo "<td>".$val->jumlah_obat	."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "<td>".$val->keterangan	."</td>";
		echo "<td>".$val->Obat->harga * $val->jumlah_obat	."</td>";
		echo "</tr>";
		$total = $total+($val->Obat->harga * $val->jumlah_obat);
	}
	
?>
	<tr>
		<td colspan="4">Total Biaya Penggunaan Alat Kesehatan</td>
		<td><?php echo $total; ?></td>
	</tr>
</table>
<br>


<table class="other-table" border="1" cellpadding="5" style="width:100%">
	<tr>
		<td>Tindakan</td>
		<td>Harga</td>
		<td>Tanggal</td>
	</tr>
<?php
	$total_tindakan = 0;
	foreach($model_detail_tindakan as $val)
	{
		echo "<tr>";
		echo "<td>".$val->Tindakan->tindakan."</td>";
		echo "<td>".$val->Tindakan->harga."</td>";
		echo "<td>".$val->tanggal	."</td>";
		echo "</tr>";
		$total_tindakan = $total_tindakan+($val->Tindakan->harga);
	}
	
?>
	<tr>
		<td colspan="2">Total Biaya Tindakan</td>
		<td><?php echo $total_tindakan; ?></td>
	</tr>
	<tr>
		<td colspan="2">Total Keseluruhan Biaya</td>
		<td><?php echo number_format($lama*$model->Kamar->harga+$total+$total_tindakan, 2, ',', '.'); ?></td>
	</tr>
</table>