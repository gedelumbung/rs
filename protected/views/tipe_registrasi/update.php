<?php
/* @var $this Tipe_registrasiController */
/* @var $model TipeRegistrasi */

$this->breadcrumbs=array(
	'Tipe Registrasis'=>array('index'),
	$model->id_tipe_registrasi=>array('view','id'=>$model->id_tipe_registrasi),
	'Update',
);

$this->menu=array(
	array('label'=>'List TipeRegistrasi', 'url'=>array('index')),
	array('label'=>'Create TipeRegistrasi', 'url'=>array('create')),
	array('label'=>'View TipeRegistrasi', 'url'=>array('view', 'id'=>$model->id_tipe_registrasi)),
	array('label'=>'Manage TipeRegistrasi', 'url'=>array('admin')),
);
?>

<h1>Update TipeRegistrasi <?php echo $model->id_tipe_registrasi; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>