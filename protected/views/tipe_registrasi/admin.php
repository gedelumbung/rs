<?php
/* @var $this Tipe_registrasiController */
/* @var $model TipeRegistrasi */

$this->breadcrumbs=array(
	'Tipe Registrasis'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TipeRegistrasi', 'url'=>array('index')),
	array('label'=>'Create TipeRegistrasi', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tipe-registrasi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tipe Registrasi</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipe-registrasi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tipe_registrasi',
		'tipe_registrasi',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
