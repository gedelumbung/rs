<?php
/* @var $this Tipe_registrasiController */
/* @var $model TipeRegistrasi */

$this->breadcrumbs=array(
	'Tipe Registrasis'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TipeRegistrasi', 'url'=>array('index')),
	array('label'=>'Manage TipeRegistrasi', 'url'=>array('admin')),
);
?>

<h1>Create TipeRegistrasi</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>