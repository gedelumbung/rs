<?php
/* @var $this Tipe_registrasiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipe Registrasis',
);

$this->menu=array(
	array('label'=>'Create TipeRegistrasi', 'url'=>array('create')),
	array('label'=>'Manage TipeRegistrasi', 'url'=>array('admin')),
);
?>

<h1>Tipe Registrasis</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
