<?php
/* @var $this Tipe_registrasiController */
/* @var $model TipeRegistrasi */

$this->breadcrumbs=array(
	'Tipe Registrasis'=>array('index'),
	$model->id_tipe_registrasi,
);

$this->menu=array(
	array('label'=>'List TipeRegistrasi', 'url'=>array('index')),
	array('label'=>'Create TipeRegistrasi', 'url'=>array('create')),
	array('label'=>'Update TipeRegistrasi', 'url'=>array('update', 'id'=>$model->id_tipe_registrasi)),
	array('label'=>'Delete TipeRegistrasi', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tipe_registrasi),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TipeRegistrasi', 'url'=>array('admin')),
);
?>

<h1>View TipeRegistrasi #<?php echo $model->id_tipe_registrasi; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tipe_registrasi',
		'tipe_registrasi',
	),
)); ?>
