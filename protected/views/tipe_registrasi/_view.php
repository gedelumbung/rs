<?php
/* @var $this Tipe_registrasiController */
/* @var $data TipeRegistrasi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipe_registrasi')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tipe_registrasi), array('view', 'id'=>$data->id_tipe_registrasi)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipe_registrasi')); ?>:</b>
	<?php echo CHtml::encode($data->tipe_registrasi); ?>
	<br />


</div>