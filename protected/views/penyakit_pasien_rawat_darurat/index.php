<?php

$this->menu=array(
	array('label'=>'Penyakit Pasien Rawat Inap', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tindakan-rawat-jalan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<form method="post" action="<?php echo Yii::app()->baseUrl; ?>/penyakit_pasien_rawat_darurat/excel">
        <select name="bulan">
                <option value=''>Pilih Bulan</option>
                <?php
                        for($i=1;$i<=12;$i++)
                        {
                                if($i<10)
                                {
                                        echo '<option value="0'.$i.'">0'.$i.'</option>';
                                }
                                else
                                {
                                        echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                        }
                ?>
        </select>
        <select name="tahun">
                <?php
                        for($i=2013;$i<=date('Y')+1;$i++)
                        {
                                echo '<option value="'.$i.'">'.$i.'</option>';
                        }
                ?>
        </select>
        <input type="submit" value="Export to Excel" class="button">
</form>
<h1>Laporan Penyakit Pasien Rawat Darurat</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php

	function loadModelLogPenyakit($id)
	{
      $val = '';
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
		$model=LogPenyakitRawatDarurat::model()->findAll($criteria);
		foreach ($model as $value) {
		      $val .= '<li>'.$value->Penyakit->penyakit.'</li>';
		}
		return $val;
	}

	 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tindakan-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_tr_tindakan_rawat_darurat',
        array( 
        	'header'=>'Nama Pasien', 
        	'name'=>'nama_pasien', 
        	'value'=>'$data->Pasien->nama' 
        	),
		'TipeRegistrasi.tipe_registrasi',
        array( 
                'header'=>'Penyakit', 
                'value'=>'loadModelLogPenyakit($data->id_tr_tindakan_rawat_darurat)',
                'type'=>'raw'
                ),
		'tgl_masuk',
		'jam_masuk',
	),
)); ?>
