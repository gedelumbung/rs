<div class="page-header">
				<div class="pull-left">
					<h4><i class="icon-home"></i> Dashboard</h4>
				</div>
				<div class="pull-right">
					<ul class="bread">
						<li><a href="dashboard.html">Home</a><span class="divider">/</span></li>
						<li class='active'>Dashboard</li>
					</ul>
				</div>
			</div>
			<div class="content-highlighted">
				<ul class="quick" data-collapse="collapse">
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/statistics.png" alt="" /><span>Pendaftaran</span></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/order-149.png" alt="" /><span>Rawat Jalan</span></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/shipping.png" alt="" /><span>Rawat Darurat</span></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/my-account.png" alt="" /><span>Rawat Inap</span></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/full-time.png" alt="" /><span>Pembayaran</span></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/asset/img/icons/date.png" alt="" /><span>Laporan</span></a>
					</li>
				</ul>
			</div>

			<div class="container-fluid" id="content-area">
				
				<div class="row-fluid">

					<div class="span12">
						<div class="box">
							<div class="box-head">
								<i class="icon-comments"></i>
								<span>Selamat Datang, <b><?php echo Yii::app()->user->nama_lengkap; ?></b></span>
								<div class="actions">
									<a href="#" rel='tooltip' title="Save conversation"><i class="icon-user"></i></a>
									<a href="#" rel='tooltip' title="Show history" data-placement="left"><i class="icon-signout"></i></a>
								</div>
							</div>
							<div class="box-body box-body-nopadding">
								<ul class="messages">
									<li class="left">
										<div class="message">
											<span class="name">Name : <?php echo Yii::app()->user->nama_lengkap; ?></span>
											<p></p>
											<span class="name">Username : <?php echo Yii::app()->user->username; ?></span>
											<p>Selamat datang <?php echo Yii::app()->user->nama_lengkap; ?>, akses anda sebagai admin.</p>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</div>