<?php
        function loadModelLogDokter($id)
        {
                $val = '';
                $criteria = new CDbCriteria();
                $criteria->condition = "id_tindakan_rawat_darurat = '".$id."'";
                $model=LogDokterRawatDarurat::model()->findAll($criteria);
                foreach ($model as $value) {
                        $val .= $value->Dokter->nama_dokter.', ';
                }
                return $val;
        }
?>

<?php $this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'pemakaian-alat-rawat-darurat-grid',
	'dataProvider'=>$model->search_report_bulan(),
	'columns'=>array(
                'TindakanRawatDarurat.Pasien.no_rm',
        array( 
                'header'=>'Nama Dokter', 
                'value'=>'loadModelLogDokter($data->TindakanRawatDarurat->id_tr_tindakan_rawat_darurat)',
                'type'=>'raw'
                ),
        array( 
                'header'=>'Registrasi', 
                'name'=>'id_tipe_registrasi', 
                'value'=>'$data->TindakanRawatDarurat->TipeRegistrasi->tipe_registrasi' 
                ),
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->nama' 
        	),
        array( 
        	'header'=>'Tanggal Lahir', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->tgl_lahir' 
        	),
        array( 
        	'header'=>'Alamat', 
        	'value'=>'$data->TindakanRawatDarurat->Pasien->alamat' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'Obat.harga',
        array( 
        	'header'=>'Jumlah Total', 
        	'value'=>'$data->jumlah_obat * $data->Obat->harga' 
        	),
	),
)); ?>
