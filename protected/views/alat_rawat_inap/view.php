<?php
/* @var $this Alat_rawat_inapController */
/* @var $model PemakaianAlatRawatInap */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inaps'=>array('index'),
	$model->id_pemakaian_alat_rawat_inap,
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatInap', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatInap', 'url'=>array('create')),
	array('label'=>'Update PemakaianAlatRawatInap', 'url'=>array('update', 'id'=>$model->id_pemakaian_alat_rawat_inap)),
	array('label'=>'Delete PemakaianAlatRawatInap', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_pemakaian_alat_rawat_inap),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PemakaianAlatRawatInap', 'url'=>array('admin')),
);
?>

<h1>View PemakaianAlatRawatInap #<?php echo $model->id_pemakaian_alat_rawat_inap; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_pemakaian_alat_rawat_inap',
		'TindakanRawatInap.Pasien.nama',
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		'keterangan',
	),
)); ?>
