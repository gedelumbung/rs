<?php
/* @var $this Alat_rawat_inapController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inaps',
);

$this->menu=array(
	array('label'=>'Create PemakaianAlatRawatInap', 'url'=>array('create')),
	array('label'=>'Manage PemakaianAlatRawatInap', 'url'=>array('admin')),
);
?>

<h1>Pemakaian Alat Rawat Inaps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
