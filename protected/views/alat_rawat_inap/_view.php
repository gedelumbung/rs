<?php
/* @var $this Alat_rawat_inapController */
/* @var $data PemakaianAlatRawatInap */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pemakaian_alat_rawat_inap')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pemakaian_alat_rawat_inap), array('view', 'id'=>$data->id_pemakaian_alat_rawat_inap)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_darurat')); ?>:</b>
	<?php echo CHtml::encode($data->id_tr_tindakan_rawat_darurat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_obat')); ?>:</b>
	<?php echo CHtml::encode($data->id_obat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_obat')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_obat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan); ?>
	<br />


</div>