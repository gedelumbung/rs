<?php
/* @var $this Alat_rawat_inapController */
/* @var $model PemakaianAlatRawatInap */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inaps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatInap', 'url'=>array('index')),
	array('label'=>'Manage PemakaianAlatRawatInap', 'url'=>array('admin')),
);
?>

<h1>Create PemakaianAlatRawatInap</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>