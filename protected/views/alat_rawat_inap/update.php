<?php
/* @var $this Alat_rawat_inapController */
/* @var $model PemakaianAlatRawatInap */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inaps'=>array('index'),
	$model->id_pemakaian_alat_rawat_inap=>array('view','id'=>$model->id_pemakaian_alat_rawat_inap),
	'Update',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatInap', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatInap', 'url'=>array('create')),
	array('label'=>'View PemakaianAlatRawatInap', 'url'=>array('view', 'id'=>$model->id_pemakaian_alat_rawat_inap)),
	array('label'=>'Manage PemakaianAlatRawatInap', 'url'=>array('admin')),
);
?>

<h1>Update PemakaianAlatRawatInap <?php echo $model->id_pemakaian_alat_rawat_inap; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>