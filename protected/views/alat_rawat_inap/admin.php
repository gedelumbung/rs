<?php
/* @var $this Alat_rawat_inapController */
/* @var $model PemakaianAlatRawatInap */

$this->breadcrumbs=array(
	'Pemakaian Alat Rawat Inaps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PemakaianAlatRawatInap', 'url'=>array('index')),
	array('label'=>'Create PemakaianAlatRawatInap', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pemakaian-alat-rawat-inap-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pemakaian Alat Rawat Inap</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pemakaian-alat-rawat-inap-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_pemakaian_alat_rawat_inap',
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatInap->Pasien->nama' 
        	),
		'Obat.nama_obat',
		'jumlah_obat',
		'tanggal',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
