<?php

	function loadModelLogPenyakit($id)
	{
      $val = '';
		$criteria = new CDbCriteria();
		$criteria->condition = "id_tindakan_rawat_inap = '".$id."'";
		$model=LogPenyakitRawatInap::model()->findAll($criteria);
		foreach ($model as $value) {
		      $val .= $value->Penyakit->penyakit.', ';
		}
		return $val;
	}

$this->widget('ext.phpexcel.EExcelView', array(
	'id'=>'tindakan-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id_tr_tindakan_rawat_inap',
        array( 
        	'header'=>'Nama Pasien', 
        	'name'=>'nama_pasien', 
        	'value'=>'$data->Pasien->nama' 
        	),
		'TipeRegistrasi.tipe_registrasi',
        array( 
                'header'=>'Penyakit', 
                'value'=>'loadModelLogPenyakit($data->id_tr_tindakan_rawat_inap)',
                'type'=>'raw'
                ),
		'tgl_masuk',
		'jam_masuk',
	),
)); ?>
