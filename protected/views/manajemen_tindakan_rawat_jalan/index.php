<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Jalans',
);

$this->menu=array(
	array('label'=>'Create ManajamenTindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'Manage ManajamenTindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Manajamen Tindakan Rawat Jalans</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
