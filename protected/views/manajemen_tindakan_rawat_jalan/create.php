<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $model ManajamenTindakanRawatJalan */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Jalans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Manage ManajamenTindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Create ManajamenTindakanRawatJalan</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>