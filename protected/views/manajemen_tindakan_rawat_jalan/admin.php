<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $model ManajamenTindakanRawatJalan */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Jalans'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatJalan', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manajamen-tindakan-rawat-jalan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Manajamen Tindakan Rawat Jalan</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manajamen-tindakan-rawat-jalan-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tindakan_rawat_jalan',
        array( 
        	'header'=>'Nama Pasien', 
        	'value'=>'$data->TindakanRawatJalan->Pasien->nama' 
        	),
		'Tindakan.tindakan',
		'tanggal',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
