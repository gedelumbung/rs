<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $model ManajamenTindakanRawatJalan */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Jalans'=>array('index'),
	$model->id_tindakan_rawat_jalan,
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'Update ManajamenTindakanRawatJalan', 'url'=>array('update', 'id'=>$model->id_tindakan_rawat_jalan)),
	array('label'=>'Delete ManajamenTindakanRawatJalan', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_tindakan_rawat_jalan),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ManajamenTindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>View ManajamenTindakanRawatJalan #<?php echo $model->id_tindakan_rawat_jalan; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_tindakan_rawat_jalan',
		'TindakanRawatJalan.Pasien.nama',
		'Tindakan.tindakan',
		'tanggal',
	),
)); ?>
