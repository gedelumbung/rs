<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $data ManajamenTindakanRawatJalan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tindakan_rawat_jalan')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_tindakan_rawat_jalan), array('view', 'id'=>$data->id_tindakan_rawat_jalan)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tr_tindakan_rawat_jalan')); ?>:</b>
	<?php echo CHtml::encode($data->id_tr_tindakan_rawat_jalan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal); ?>
	<br />


</div>