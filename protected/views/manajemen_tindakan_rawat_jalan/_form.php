<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $model ManajamenTindakanRawatJalan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manajamen-tindakan-rawat-jalan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_tr_tindakan_rawat_jalan'); ?>

			<?php
				$this->widget('ext.chosen.Chosen',array(
				   'name' => 'ManajamenTindakanRawatJalan[id_tr_tindakan_rawat_jalan]', // input name
				   'value' => $model->id_tr_tindakan_rawat_jalan, // selection
				   'data' => array(''=>'Semua') + CHtml::listData(TindakanRawatJalan::model()->findAll(),'id_tr_tindakan_rawat_jalan','concatened'),
				));
			?>
		<?php echo $form->error($model,'id_tr_tindakan_rawat_jalan'); ?>
	</div>
	<br>

	<div class="row">
		<?php echo $form->labelEx($model,'id_tindakan'); ?>

			<?php
				$this->widget('ext.chosen.Chosen',array(
				   'name' => 'ManajamenTindakanRawatJalan[id_tindakan]', // input name
				   'value' => $model->id_tindakan, // selection
				   'data' => array(''=>'Semua') + CHtml::listData(Tindakan::model()->findAll(),'id_tindakan','tindakan'),
				));
			?>
		<?php echo $form->error($model,'id_tindakan'); ?>
	</div>
	<br>

	<div class="row">
		<?php echo $form->labelEx($model,'tanggal'); ?>
		<?php echo $form->dateField($model,'tanggal',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'tanggal'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->