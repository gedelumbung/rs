<?php
/* @var $this Manajemen_tindakan_rawat_jalanController */
/* @var $model ManajamenTindakanRawatJalan */

$this->breadcrumbs=array(
	'Manajamen Tindakan Rawat Jalans'=>array('index'),
	$model->id_tindakan_rawat_jalan=>array('view','id'=>$model->id_tindakan_rawat_jalan),
	'Update',
);

$this->menu=array(
	array('label'=>'List ManajamenTindakanRawatJalan', 'url'=>array('index')),
	array('label'=>'Create ManajamenTindakanRawatJalan', 'url'=>array('create')),
	array('label'=>'View ManajamenTindakanRawatJalan', 'url'=>array('view', 'id'=>$model->id_tindakan_rawat_jalan)),
	array('label'=>'Manage ManajamenTindakanRawatJalan', 'url'=>array('admin')),
);
?>

<h1>Update ManajamenTindakanRawatJalan <?php echo $model->id_tindakan_rawat_jalan; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>